/* CHM  20/10/2019 */
/* Suma_amtrices  */
/* Seminario 0*/
/* Ejercicio 2*/


#include <stdio.h>
#include <stdlib.h>

#define FIL  125
#define COL  125 /*tamaño maximo*/

int i,j,pos;

void inicializa_matrices(float   * M_Res,float * M_A, float * M_B)
{
	
	 for(i=0;i<FIL;i++)
    {
        for(j=0;j<COL;j++)				
        {
			pos=i*COL+j; 
            M_A[pos]=i+j; // valores para A
			M_B[pos]=j+1; // valores para B
		    M_Res[pos]=0; 
        }
    }
	
}

void suma_matrices(float   * M_Res,float * M_A,  float * M_B)
{
	
	 for(i=0;i<FIL;i++)
    {
        for(j=0;j<COL;j++)
        {
            //Suma
			pos=i*COL+j;
		    M_Res[pos]=M_A[pos]+M_B[pos]; 
        }
    }
	
}

void imprime_matrices(float   * M_Res)
{
    /* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(i=0;i<FIL;i++)
    {
        for(j=0;j<COL;j++) {
			pos=i*COL+j;
			 printf("%10f",M_Res[pos]);
		}
        printf("\n");
    }
}
	

int main(void)
{
	  
								
float   * M_Res, * M_A, * M_B;

    printf("Programa que suma matrices:\n\n"); 
	printf("Reservando memoria :\n\n");

    M_Res = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_Res == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	   
	M_A = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	M_B = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_B == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	printf("Dando valores a las matrices :\n\n");
    inicializa_matrices(    M_Res,  M_A,   M_B);  
  
    printf("Sumando matrices :\n\n");
    suma_matrices(M_Res,  M_A, M_B);  
      
    printf("Muestra REsultado matrices :\n\n");
    imprime_matrices(M_Res);  
	
	//liberamos memoria
	free(M_A);
	free(M_B);
	free(M_Res);
	
    return 0;
}

