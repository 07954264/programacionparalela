/* CHM  20/10/2019 */
/* Patrones  STENCIL  */
/* Seminario 0*/
/* Ejercicio 1  */


#include <stdio.h>
#include <stdlib.h>

#define N  125
#define NUM_ITER  125 /*ITERACIONES*/

int i,j,pos;

void inicializa_matrices(float   * M_Res,float * M_A)
{
	
	 for(i=0;i<N;i++)         
        {			 
            M_A[i]=5.0; // valores para A			
		    M_Res[i]=0.0; 
        }
    
	
}

void itera(float * M_Res,float * M_A)
{
	float   * M_aux;
	
	 M_aux = (float *) malloc(N*sizeof(float));
	 if (M_aux == NULL) 
	    {
		  fprintf(stderr,"Error al reservar memoria \n");		
	    }
	   
	 for(i=0;i<NUM_ITER ;i++)
     {
        for(j=1;j<N-1;j++)
        {
            //Suma
			    M_Res[j]=0.3*(M_A[j-1]+M_A[j]+M_A[j+1]); 
        }
		M_aux=M_A;
		M_A=M_Res;
		M_Res=M_aux;
    }
	
	free(M_aux);
}

void imprime_matrices(float   * M_Res)
{
    /* Mostramos el resultado */
    printf("La matriz resultado es:\n\n");
   for(i=1;i<N-1;i++)
    {
       
			 printf("Elemento: %10i valor:%10f",i,M_Res[i]);
	
        printf("\n");
    }
 printf("final");
    
}
	

int main(void)
{
	  
								
float   * M_Res, * M_A;

    printf("Programa Ejercicio 1 seminario 0:\n\n"); 
	printf("Reservando memoria :\n\n");

    M_Res = (float *) malloc(N*sizeof(float));
	
	if (M_Res == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		
	   }
	   
	M_A = (float *) malloc(N*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	
	
	printf("Dando valores a las matrices :\n\n");
    inicializa_matrices(    M_Res,  M_A);  
  
   printf("Itera matrices :\n\n");
   itera(M_Res,  M_A);  
      
   printf("Muestra REsultado matrices :\n\n");
   imprime_matrices(M_A);  
	
	//liberamos memoria
	//free(M_A);

	//free(M_Res);
	
    return 0;
}

