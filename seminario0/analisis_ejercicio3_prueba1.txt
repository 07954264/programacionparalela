*** File /home/ProgParalelaOnl/07954264/programacionparalela/programacionparalela/seminario0/ejercicio3.c:
                /* CHM  20/10/2019 */
                /* METODO JACOBI  */
                /* Seminario 0*/
                /* Ejercicio 3 */
                
                #include <stdio.h>
                #include <stdlib.h>
                
                #define FIL  125
                #define COL  125 /*tamaño maximo*/
                #define NUM_ITER 4096
                
                int i,j,k,pos,
                          posI_ant,
                		  posI_pos,
                		  posJ_ant,
                		  posJ_pos;
                
                void inicializa_matrices(float   * M_Res,float * M_A)
           1 -> {
                	
                	 for(i=0;i<FIL;i++)
                    {
                        for(j=0;j<COL;j++)				
                        {
                			pos=i*COL+j; 
                			if (pos%2 == 0) 
                               M_A[pos]=75.0; // valores para A
                			else
                			   M_A[pos]=150.0; // valores para A	
                			M_Res[pos]=0.0; 
                        }
                    }
                	
                }
                
                void metodo_jacobi(float   * M_Res,float * M_A)
           1 -> {
                	
                	 float   * M_aux;
                	
                	 M_aux = (float *) malloc(COL*FIL*sizeof(float));
                	 
                	 if (M_aux == NULL) 
                	    {
                		 fprintf(stderr,"Error al reservar memoria \n");
                	     }
                	   
                	
                	 for(k=0;k<NUM_ITER ;k++)
                    {
                       for(i=1;i<FIL-1;i++)
                        {
                         for(j=1;j<COL-1;j++)
                          {        
                			pos=i*COL+j;
                			
                			posI_ant=(i-1)*COL+j;
                			posI_pos=(i+1)*COL+j;
                			posJ_ant=i*COL+(j+1);
                			posJ_pos=i*COL+(j-1);
                			
                		    M_Res[pos]=0.3*(M_A[pos]+M_A[posI_ant]
                			                        +M_A[posI_pos]
                									+M_A[posJ_ant]
                                                    +M_A[posJ_pos]
                							); 
                         }
                		} 
                	     	//M_aux=M_Res;
                		    M_A=M_Res;
                		    //M_Res=M_aux;
                    }
                	
                	 free(M_aux);
                	
                }
                
                void imprime_matrices(float   * M_Res)
           1 -> {
                    /* Mostramos el resultado */
                    printf("\n\nLa matriz resultado es:\n\n");
                    for(i=0;i<FIL;i++)
                    {
                        for(j=0;j<COL;j++) {
                			pos=i*COL+j;
                			 printf("%10f",M_Res[pos]);
                		}
                        printf("\n");
                    }
                }
                	
                
                int main(void)
       ##### -> {
                	  
                								
                float   * M_Res, * M_A, * M_B;
                
                    printf("Programa JACOBI :\n\n"); 
                	printf("Reservando memoria :\n\n");
                
                    M_Res = (float *) malloc(FIL*COL*sizeof(float));
                	
                	if (M_Res == NULL) 
                	    {
                		fprintf(stderr,"Error al reservar memoria \n");
                		return -1;		
                	   }
                	   
                	M_A = (float *) malloc(FIL*COL*sizeof(float));
                	
                	if (M_A == NULL) 
                	    {
                		fprintf(stderr,"Error al reservar memoria \n");
                		return -1;		
                	   }
                		
                	printf("Dando valores a las matrices :\n\n");
                    inicializa_matrices(M_Res,M_A);  
                  
                    printf("Metodo JACOBI :\n\n");
                    metodo_jacobi(M_Res,  M_A);  
                      
                    printf("Muestra REsultado matrices :\n\n");
                    imprime_matrices(M_Res);  
                	
                	//liberamos memoria
                	free(M_A);
                	free(M_Res);
                	
                    return 0;
                }
                


Top 10 Lines:

     Line      Count

       20          1
       38          1
       80          1

Execution Summary:

       57   Executable lines in this file
        4   Lines executed
     7.02   Percent of the file executed

        3   Total number of line executions
     0.05   Average executions per line
Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  Ts/call  Ts/call  name    
 24.50      0.20     0.20                             metodo_jacobi (ejercicio3.c:63 @ 400a17)
 18.37      0.35     0.15                             metodo_jacobi (ejercicio3.c:63 @ 400aca)
 13.47      0.46     0.11                             metodo_jacobi (ejercicio3.c:65 @ 400a8a)
  9.80      0.54     0.08                             metodo_jacobi (ejercicio3.c:58 @ 4009af)
  8.57      0.61     0.07                             metodo_jacobi (ejercicio3.c:66 @ 400aaa)
  6.12      0.66     0.05                             metodo_jacobi (ejercicio3.c:60 @ 4009e3)
  4.90      0.70     0.04                             metodo_jacobi (ejercicio3.c:54 @ 400ae2)
  3.67      0.73     0.03                             metodo_jacobi (ejercicio3.c:56 @ 400998)
  3.67      0.76     0.03                             metodo_jacobi (ejercicio3.c:61 @ 4009fd)
  3.67      0.79     0.03                             metodo_jacobi (ejercicio3.c:64 @ 400a6a)
  2.45      0.81     0.02                             metodo_jacobi (ejercicio3.c:59 @ 4009c9)
  1.22      0.82     0.01                             metodo_jacobi (ejercicio3.c:52 @ 400b00)
  0.00      0.82     0.00        1     0.00     0.00  imprime_matrices (ejercicio3.c:80 @ 400b55)
  0.00      0.82     0.00        1     0.00     0.00  inicializa_matrices (ejercicio3.c:20 @ 400826)
  0.00      0.82     0.00        1     0.00     0.00  metodo_jacobi (ejercicio3.c:38 @ 400923)

 %         the percentage of the total running time of the
time       program used by this function.

cumulative a running sum of the number of seconds accounted
 seconds   for by this function and those listed above it.

 self      the number of seconds accounted for by this
seconds    function alone.  This is the major sort for this
           listing.

calls      the number of times this function was invoked, if
           this function is profiled, else blank.

 self      the average number of milliseconds spent in this
ms/call    function per call, if this function is profiled,
	   else blank.

 total     the average number of milliseconds spent in this
ms/call    function and its descendents per call, if this
	   function is profiled, else blank.

name       the name of the function.  This is the minor sort
           for this listing. The index shows the location of
	   the function in the gprof listing. If the index is
	   in parenthesis it shows where it would appear in
	   the gprof listing if it were to be printed.

Copyright (C) 2012-2015 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

		     Call graph (explanation follows)


granularity: each sample hit covers 2 byte(s) for 1.21% of 0.82 seconds

index % time    self  children    called     name
                0.00    0.00       1/1           main (ejercicio3.c:125 @ 400ce7) [54]
[13]     0.0    0.00    0.00       1         imprime_matrices (ejercicio3.c:80 @ 400b55) [13]
-----------------------------------------------
                0.00    0.00       1/1           main (ejercicio3.c:120 @ 400cb7) [51]
[14]     0.0    0.00    0.00       1         inicializa_matrices (ejercicio3.c:20 @ 400826) [14]
-----------------------------------------------
                0.00    0.00       1/1           main (ejercicio3.c:123 @ 400cd4) [53]
[15]     0.0    0.00    0.00       1         metodo_jacobi (ejercicio3.c:38 @ 400923) [15]
-----------------------------------------------

 This table describes the call tree of the program, and was sorted by
 the total amount of time spent in each function and its children.

 Each entry in this table consists of several lines.  The line with the
 index number at the left hand margin lists the current function.
 The lines above it list the functions that called this function,
 and the lines below it list the functions this one called.
 This line lists:
     index	A unique number given to each element of the table.
		Index numbers are sorted numerically.
		The index number is printed next to every function name so
		it is easier to look up where the function is in the table.

     % time	This is the percentage of the `total' time that was spent
		in this function and its children.  Note that due to
		different viewpoints, functions excluded by options, etc,
		these numbers will NOT add up to 100%.

     self	This is the total amount of time spent in this function.

     children	This is the total amount of time propagated into this
		function by its children.

     called	This is the number of times the function was called.
		If the function called itself recursively, the number
		only includes non-recursive calls, and is followed by
		a `+' and the number of recursive calls.

     name	The name of the current function.  The index number is
		printed after it.  If the function is a member of a
		cycle, the cycle number is printed between the
		function's name and the index number.


 For the function's parents, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the function into this parent.

     children	This is the amount of time that was propagated from
		the function's children into this parent.

     called	This is the number of times this parent called the
		function `/' the total number of times the function
		was called.  Recursive calls to the function are not
		included in the number after the `/'.

     name	This is the name of the parent.  The parent's index
		number is printed after it.  If the parent is a
		member of a cycle, the cycle number is printed between
		the name and the index number.

 If the parents of the function cannot be determined, the word
 `<spontaneous>' is printed in the `name' field, and all the other
 fields are blank.

 For the function's children, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the child into the function.

     children	This is the amount of time that was propagated from the
		child's children to the function.

     called	This is the number of times the function called
		this child `/' the total number of times the child
		was called.  Recursive calls by the child are not
		listed in the number after the `/'.

     name	This is the name of the child.  The child's index
		number is printed after it.  If the child is a
		member of a cycle, the cycle number is printed
		between the name and the index number.

 If there are any cycles (circles) in the call graph, there is an
 entry for the cycle-as-a-whole.  This entry shows who called the
 cycle (as parents) and the members of the cycle (as children.)
 The `+' recursive calls entry shows the number of function calls that
 were internal to the cycle, and the calls entry for each member shows,
 for that member, how many times it was called from other members of
 the cycle.

Copyright (C) 2012-2015 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

Index by function name

  [13] imprime_matrices (ejercicio3.c:80 @ 400b55) [11] metodo_jacobi (ejercicio3.c:59 @ 4009c9) [3] metodo_jacobi (ejercicio3.c:65 @ 400a8a)
  [14] inicializa_matrices (ejercicio3.c:20 @ 400826) [6] metodo_jacobi (ejercicio3.c:60 @ 4009e3) [5] metodo_jacobi (ejercicio3.c:66 @ 400aaa)
  [15] metodo_jacobi (ejercicio3.c:38 @ 400923) [9] metodo_jacobi (ejercicio3.c:61 @ 4009fd) [2] metodo_jacobi (ejercicio3.c:63 @ 400aca)
   [8] metodo_jacobi (ejercicio3.c:56 @ 400998) [1] metodo_jacobi (ejercicio3.c:63 @ 400a17) [7] metodo_jacobi (ejercicio3.c:54 @ 400ae2)
   [4] metodo_jacobi (ejercicio3.c:58 @ 4009af) [10] metodo_jacobi (ejercicio3.c:64 @ 400a6a) [12] metodo_jacobi (ejercicio3.c:52 @ 400b00)
