/* CHM  20/10/2019 */
/* METODO JACOBI  */
/* Seminario 0*/
/* Ejercicio 3 */

#include <stdio.h>
#include <stdlib.h>

#define FIL  500
#define COL  500 /*tamaño maximo*/
#define NUM_ITER 4096

int i,j,k,pos,
          posI_ant,
		  posI_pos,
		  posJ_ant,
		  posJ_pos;

void inicializa_matrices(float   * M_Res,float * M_A)
{
	
	 for(i=0;i<FIL;i++)
    {
        for(j=0;j<COL;j++)				
        {
			pos=i*COL+j; 
			if (pos%2 == 0) 
               M_A[pos]=75.0; // valores para A
			else
			   M_A[pos]=150.0; // valores para A	
			M_Res[pos]=0.0; 
        }
    }
	
}

void metodo_jacobi(float   * M_Res,float * M_A)
{
	
	 float   * M_aux;
	
	 
	   
	
	 for(k=0;k<NUM_ITER ;k++)
    {
       for(i=1;i<FIL-1;i++)
        {
         for(j=1;j<COL-1;j++)
          {        
			pos=i*COL+j;
			
			posI_ant=(i-1)*COL+j;
			posI_pos=(i+1)*COL+j;
			posJ_ant=i*COL+(j+1);
			posJ_pos=i*COL+(j-1);
			
		    M_Res[pos]=0.3*(M_A[pos]+M_A[posI_ant]
			                        +M_A[posI_pos]
									+M_A[posJ_ant]
                                    +M_A[posJ_pos]
							); 
         }
		} 
	    	M_aux=M_Res;
		    M_A=M_Res;
		M_Res=M_aux;
    }
	

	
}

void imprime_matrices(float   * M_Res)
{
    /* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(i=0;i<FIL;i++)
    {
        for(j=0;j<COL;j++) {
			pos=i*COL+j;
			 printf("%10f",M_Res[pos]);
		}
        printf("\n");
    }
}
	

int main(void)
{
	  
								
float   * M_Res, * M_A, * M_B;

    printf("Programa JACOBI :\n\n"); 
	printf("Reservando memoria :\n\n");

    M_Res = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_Res == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	   
	M_A = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
		
	printf("Dando valores a las matrices :\n\n");
    inicializa_matrices(M_Res,M_A);  
  
    printf("Metodo JACOBI :\n\n");
    metodo_jacobi(M_Res,  M_A);  
      
    printf("Muestra REsultado matrices :\n\n");
    imprime_matrices(M_Res);  
	
	//liberamos memoria
	free(M_A);
	free(M_Res);
	
    return 0;
}

