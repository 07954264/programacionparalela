/* CHM  20/10/2019 */
/* Suma_vectores  */
/* Seminario 0*/
/* Ejercicio 4*/


#include <stdio.h>
#include <stdlib.h>

#define N 125


int i;
float Resultado_suma;

void inicializa_matrices(float * M_A)
{
	
	 for(i=0;i<N;i++)	 
            M_A[i]=i; // valores para A        
			
      
	
}

float suma_vector(float * M_A)
{
  float	Resultado = 0;
	 for(i=0;i<N;i++)
    	    Resultado=M_A[i]+Resultado;  
		
	  
 return Resultado;
}


	

int main(void)
{
	  
								
float    * M_A;

    printf("Programa que suma elementos de un vector :\n\n"); 
	printf("Reservando memoria :\n\n");
       
	M_A = (float *) malloc(N*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	printf("Dando valores a las matriz :\n\n");
    inicializa_matrices( M_A);  
  
    printf("Sumando vectores :\n\n");
    Resultado_suma=suma_vector(M_A);  
     
     printf("%10f",Resultado_suma);	 
    
	
	//liberamos memoria
	free(M_A);
	
    return 0;
}

