﻿#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <sys/time.h>
#include "wtime.h"
#include "definitions.h"
#include "energy_struct.h"
#include "cuda_runtime.h"
#include "solver.h"

using namespace std;

/**
* Kernel del calculo de la solvation. Se debe anadir los parametros 
*/
__global__ void escalculation (int atoms_r, int atoms_l, int nlig, double *rec_x_d, double *rec_y_d, double *rec_z_d, double *lig_x_d, double *lig_y_d, double *lig_z_d, double *ql_d,double *qr_d, double *energy_d, int nconformations)
{
 __shared__ double idata[1024];
 __shared__ double resultado;
 
    int row =	blockIdx.y * blockDim.y + threadIdx.y;
    int col =   blockIdx.x * blockDim.x + threadIdx.x;
 
 int tidx = threadIdx.x;
 int k;
 
 
 double dist = 0;
 
 double temp[1024];



idata[tidx]=0;
temp[tidx]=0;
 

      

 if  (col < atoms_r && row < atoms_l)
 {
 for ( k=0; k < nconformations*nlig; k+=nlig)  
  {    
        idata[tidx]=0;
		temp[tidx]=0;    
       //CALCULAMOS EL VALOR PARA UN BLOQUE
     
		dist=calculaDistancia (rec_x_d[col], rec_y_d[col], rec_z_d[col], lig_x_d[row+k], lig_y_d[row+k], lig_z_d[row+k]);
		 temp[tidx]  += (ql_d[row]* qr_d[col]) / dist;
             
	 
          idata[tidx]= temp[tidx];
     		  __syncthreads();
       
		
	   //LO REDUCIMOS	
	  //reducion en bloques de 2
		for (int s = blockDim.x / 2; s > 0; s >>= 1) 
		{
			if (tidx < s) 
			{
                idata[tidx] += idata[tidx + s];
			}
			__syncthreads();
		}
  
    
	 
		// write result for this block to global mem
		//OBTENEMOS EL VALOR EN resultado , todos los bloques.
		if (tidx == 0) 
		{
		  
		     {
			  //energy_d[k/nlig] = idata[0];
			    atomicAdd(&energy_d[k/nlig], idata[0]);
			  // if (k > 0)
			    // printf("Soy  Bloque %i resultado:%f bueno %f \n",k/nlig , lig_x_d[row+k], lig_x_d[row]);
			 }
		
		
  } 
  
   } 
 }//fin IF

}

// for ( k=0; k < nconformations; k++)     { 
/*
* Funcion para manejar el lanzamiento de CUDA 
*/
void forces_GPU_AU (int atoms_r, int atoms_l, int nlig, double *rec_x, double *rec_y, double *rec_z, double *lig_x, double *lig_y, double *lig_z, double *ql ,double *qr, double *energy, int nconformations){
	
	cudaError_t cudaStatus; //variable para recoger estados de cuda
	cudaEvent_t start,stop;
	cudaEvent_t start_htd,stop_htd;
	cudaEvent_t start_dth,stop_dth;

	double mils,mils_htd,mils_dth; 

	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventCreate(&start_htd);
	cudaEventCreate(&stop_htd);
	cudaEventCreate(&start_dth);
	cudaEventCreate(&stop_dth);
	
	int size=atoms_r * sizeof(double);
	int size_l=atoms_l * sizeof(double);
	int size_lig=(atoms_l+nconformations*nlig) * sizeof(double);
    int size_ene=(nconformations) * sizeof(double);
     int tipo_device;
	 int total_hilos;
	 int hilos_bloque;
	 int anchobloquex;
	 int anchobloquey;

     tipo_device=1; //CALCULO DOUBLE   

	//seleccionamos device
	cudaSetDevice(tipo_device); 
	//0 - Tesla K40 2880nucleos Maximum number of threads per multiprocessor:  2048
    //                          Maximum number of threads per block:           1024
	
	//vs 1 - Tesla K230 o gt230

	//creamos memoria para los vectores para GPU _d (device)
	double *rec_x_d, *rec_y_d, *rec_z_d, *qr_d, *lig_x_d, *lig_y_d, *lig_z_d, *ql_d, *energy_d;
		
    //reservamos memoria para GPU
	cudaMalloc( (void **)  &rec_x_d,atoms_r * sizeof(double));
	cudaMalloc( (void **)  &rec_y_d,atoms_r * sizeof(double));
	cudaMalloc( (void **)  &rec_z_d,atoms_r * sizeof(double));
	
	cudaMalloc( (void **)  &qr_d,atoms_r * sizeof(double));
	cudaMalloc( (void **)  &ql_d,atoms_l * sizeof(double));
	
	cudaMalloc( (void **)  &lig_x_d,(atoms_r +nconformations*nlig) * sizeof(double));
	cudaMalloc( (void **)  &lig_y_d,(atoms_r +nconformations*nlig ) * sizeof(double));
	cudaMalloc( (void **)  &lig_z_d,(atoms_r +nconformations*nlig) * sizeof(double)); 
	
	cudaMalloc( (void **)  &energy_d,( nconformations) * sizeof(double)); 
	
	//pasamos datos de host to device
	cudaEventRecord(start_htd,0);
	
	cudaMemcpy(rec_x_d,rec_x,size, cudaMemcpyHostToDevice);
	cudaMemcpy(rec_y_d,rec_y,size, cudaMemcpyHostToDevice);
	cudaMemcpy(rec_z_d,rec_z,size, cudaMemcpyHostToDevice);
	
	cudaMemcpy(qr_d,qr,size, cudaMemcpyHostToDevice);
	cudaMemcpy(ql_d,ql,size_l, cudaMemcpyHostToDevice);
	
	cudaMemcpy(lig_x_d,lig_x,size_lig, cudaMemcpyHostToDevice);
	cudaMemcpy(lig_y_d,lig_y,size_lig, cudaMemcpyHostToDevice);
	cudaMemcpy(lig_z_d,lig_z,size_lig, cudaMemcpyHostToDevice);
	
	cudaMemcpy(energy_d,energy,size_ene, cudaMemcpyHostToDevice);
	
	cudaEventRecord(stop_htd,0);	  
	cudaEventSynchronize(stop_htd);
	mils_htd =0.0f;

	cudaEventElapsedTime(&mils_htd,start_htd,stop_htd);
	
	total_hilos=atoms_r*atoms_l;
	
	
	 anchobloquex = 256;
	 anchobloquey =1;
	 
	 hilos_bloque=anchobloquex*anchobloquey;
	 
	//dimgrid (x,Y)
	dim3 dimGrid(ceil((atoms_r+anchobloquex-1)/anchobloquex),ceil((atoms_l+anchobloquey-1)/anchobloquey));
    dim3 dimBlock(anchobloquex,anchobloquey); //1024 threads por bloque
	
	
	//Definir numero de hilos y bloques
	printf("bloques: %d\n", (int)ceil(total_hilos/hilos_bloque)+1);
	printf("hilos por bloque: %d\n", hilos_bloque);

	//llamamos a kernel
	cudaEventRecord(start,0);
	escalculation <<< dimGrid ,dimBlock>>> (atoms_r, atoms_l, nlig, rec_x_d, rec_y_d, rec_z_d, lig_x_d, lig_y_d, lig_z_d, ql_d, qr_d, energy_d, nconformations);
	cudaEventRecord(stop,0);	  
	cudaEventSynchronize(stop);
	mils =0.0f;

	cudaEventElapsedTime(&mils,start,stop);
	
	//control de errores kernel
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if(cudaStatus != cudaSuccess) fprintf(stderr, "Error en el kernel %d\n", cudaStatus); 

	cudaEventRecord(start_dth,0);
	//Traemos info al host
	cudaMemcpy(energy,energy_d, size_ene,cudaMemcpyDeviceToHost);
     cudaEventRecord(stop_dth,0);	  
	cudaEventSynchronize(stop_dth);
	mils_dth =0.0f;

	cudaEventElapsedTime(&mils_dth,start_dth,stop_dth);
	// para comprobar que la ultima conformacion tiene el mismo resultado que la primera
	printf("Termino electrostatico de conformacion %d es: %f\n", nconformations-1, energy[nconformations-1]); 

	//resultado varia repecto a SECUENCIAL y CUDA en 0.000002 por falta de precision con double
	//posible solucion utilizar double, probablemente bajara el rendimiento -> mas tiempo para calculo
	printf("Termino electrostatico %f\n", energy[0]);
    
	printf("Tiempo calculo %f\n", mils/1000);
	printf("Tiempo host to device %f\n", mils_htd/1000);
	printf("Tiempo device to host %f\n", mils_dth/1000);
	
	//Liberamos memoria reservada para GPU
	cudaFree(rec_x_d);
	cudaFree(rec_y_d);
	cudaFree(rec_z_d);
	
	cudaFree(qr_d);
	cudaFree(ql_d);
	
	cudaFree(lig_x_d);
	cudaFree(lig_y_d);
	cudaFree(lig_z_d);
	
	cudaFree(energy_d);
	
	
	
	
}

/**
* Distancia euclidea compartida por funcion CUDA y CPU secuencial
*/
__device__ __host__ extern double calculaDistancia (double rx, double ry, double rz, double lx, double ly, double lz) {

  double difx = rx - lx;
  double dify = ry - ly;
  double difz = rz - lz;
  double mod2x=difx*difx;
  double mod2y=dify*dify;
  double mod2z=difz*difz;
  difx=mod2x+mod2y+mod2z;
  return sqrtf(difx);
}




/**
 * Funcion que implementa el termino electrostático en CPU
 */
void forces_CPU_AU (int atoms_r, int atoms_l, int nlig, double *rec_x, double *rec_y, double *rec_z, double *lig_x, double *lig_y, double *lig_z, double *ql ,double *qr, double *energy, int nconformations){

	double dist, total_elec = 0, miatomo[3], elecTerm;
  int totalAtomLig = nconformations * nlig;

	for (int k=0; k < totalAtomLig; k+=nlig){
	  for(int i=0;i<atoms_l;i++){					
			miatomo[0] = *(lig_x + k + i);
			miatomo[1] = *(lig_y + k + i);
			miatomo[2] = *(lig_z + k + i);

			for(int j=0;j<atoms_r;j++){				
				elecTerm = 0;
        dist=calculaDistancia (rec_x[j], rec_y[j], rec_z[j], miatomo[0], miatomo[1], miatomo[2]);
//				printf ("La distancia es %lf\n", dist);
        elecTerm = (ql[i]* qr[j]) / dist;
				total_elec += elecTerm;
//        printf ("La carga es %lf\n", total_elec);
			}
		}
		
		energy[k/nlig] = total_elec;
		total_elec = 0;
  }
	printf("Termino electrostatico %f\n", energy[0]);
}


extern void solver_AU(int mode, int atoms_r, int atoms_l,  int nlig, double *rec_x, double *rec_y, double *rec_z, double *lig_x, double *lig_y, double *lig_z, double *ql, double *qr, double *energy_desolv, int nconformaciones) {

	double elapsed_i, elapsed_o;
	
	switch (mode) {
		case 0://Sequential execution
			printf("\* CALCULO ELECTROSTATICO EN CPU *\n");
			printf("**************************************\n");			
			printf("Conformations: %d\t Mode: %d, CPU\n",nconformaciones,mode);			
			elapsed_i = wtime();
			forces_CPU_AU (atoms_r,atoms_l,nlig,rec_x,rec_y,rec_z,lig_x,lig_y,lig_z,ql,qr,energy_desolv,nconformaciones);
			elapsed_o = wtime() - elapsed_i;
			printf ("CPU Processing time: %f (seg)\n", elapsed_o);
			break;
		case 1: //OpenMP execution
			printf("\* CALCULO ELECTROSTATICO EN OPENMP *\n");
			printf("**************************************\n");			
			printf("**************************************\n");			
			printf("Conformations: %d\t Mode: %d, CMP\n",nconformaciones,mode);			
			elapsed_i = wtime();
			forces_OMP_AU (atoms_r,atoms_l,nlig,rec_x,rec_y,rec_z,lig_x,lig_y,lig_z,ql,qr,energy_desolv,nconformaciones);
			elapsed_o = wtime() - elapsed_i;
			printf ("OpenMP Processing time: %f (seg)\n", elapsed_o);
			break;
		case 2: //CUDA exeuction
			printf("\* CALCULO ELECTROSTATICO EN CUDA *\n");
      printf("**************************************\n");
      printf("Conformaciones: %d\t Mode: %d, GPU\n",nconformaciones,mode);
			elapsed_i = wtime();
			forces_GPU_AU (atoms_r,atoms_l,nlig,rec_x,rec_y,rec_z,lig_x,lig_y,lig_z,ql,qr,energy_desolv,nconformaciones);
			elapsed_o = wtime() - elapsed_i;
			printf ("GPU Processing time: %f (seg)\n", elapsed_o);			
			break; 	
	  	default:
 	    	printf("Wrong mode type: %d.  Use -h for help.\n", mode);
			exit (-1);	
	} 		
}
