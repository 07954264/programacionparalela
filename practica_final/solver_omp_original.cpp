#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <sys/time.h>
#include "wtime.h"
#include "definitions.h"
#include "energy_struct.h"
#include "solver.h"

/**
* Funcion que implementa la solvatacion en openmp
*/
extern void forces_OMP_AU (int atoms_r, int atoms_l, int nlig, float *rec_x, float *rec_y, float *rec_z, float *lig_x, float *lig_y, float *lig_z, float *ql ,float *qr, float *energy, int nconformations){

  //printf(" En el fichero solver_omp.cpp se encuentra la funcion forces_omp_au que se debe implementar con la version OpenMP\n");
  float dist, total_elec = 0, miatomo[3], elecTerm;
  int totalAtomLig = nconformations * nlig;

  int k=0;
  int i=0;
  int j=0;
  float tiempo_inicio;
  float tiempo_fin;

  float rate;
  
  float repeticiones_k;
  float total_bucle_k;
float dentro_k;

float repeticiones_i;
float total_bucle_i;
float dentro_i; 

float repeticiones_j;
float total_bucle_j;
float dentro_j_p1;
float dentro_j_p2;
float dentro_j_p3;

float ancho_banda;

float dentro_operacion_k;
float dentro_operacion_i;
float dentro_operacion_j;

float dentro_calcula;

float flops,operaciones_f; 

  //omp_set_num_threads(8);
   //printf(" Numero de procesos:%d\n",omp_get_max_threads());
 
   
  
  tiempo_inicio=omp_get_wtime();
  #pragma omp parallel for private(i,k,j)  private(elecTerm,dist,miatomo) shared(lig_x,lig_y,lig_z,energy) reduction(+:total_elec)
  for ( k=0; k < totalAtomLig; k+=nlig) {
    for( i=0;i<atoms_l;i++){
      //miatomo[0] = *(lig_x + k + i);
      //miatomo[1] = *(lig_y + k + i);
      //miatomo[2] = *(lig_z + k + i);
	  
	  miatomo[0] = lig_x[ k + i];
      miatomo[1] = lig_y[ k + i];
      miatomo[2] = lig_z[ k + i];
                      
        for( j=0;j<atoms_r;j++){
          elecTerm = 0;
          dist=calculaDistancia (rec_x[j], rec_y[j], rec_z[j], miatomo[0], miatomo[1], miatomo[2]);
          elecTerm = (ql[i]* qr[j]) / dist;
          total_elec += elecTerm;
        }
     }
     energy[k/nlig] = total_elec;
     total_elec = 0;
  }
    tiempo_fin=omp_get_wtime()-tiempo_inicio;
    
	
	/* ancho de banda*/
	repeticiones_k=totalAtomLig/nlig;
	total_bucle_k= 1 + (2 * repeticiones_k) + (3 * repeticiones_k);      
	repeticiones_i=atoms_l;
	total_bucle_i= ((1 + (2 * repeticiones_i) + (2 * repeticiones_i))*repeticiones_k ); 		
	dentro_i = (6*3 + 5*3) *repeticiones_i*repeticiones_k;
	repeticiones_j=atoms_r;
	total_bucle_j= ((1 + (2 * repeticiones_j) + (2 * repeticiones_j))*repeticiones_i*repeticiones_k ); 		
	// cambiado de 13
	dentro_j_p1= (38)* repeticiones_j * repeticiones_i*repeticiones_k ; 
	dentro_calcula=(6*3*4 +  4*4 + 4) * repeticiones_j * repeticiones_i*repeticiones_k;     
	dentro_j_p2= (4+10)* repeticiones_j * repeticiones_i*repeticiones_k;  
	dentro_j_p3= (4+8)* repeticiones_j * repeticiones_i*repeticiones_k; 
	dentro_k= (6+8)* repeticiones_k;
	
    ancho_banda=	 total_bucle_k + dentro_k +  total_bucle_i + dentro_i +  total_bucle_j + dentro_j_p1 + dentro_j_p2 + dentro_j_p3 + dentro_calcula;
     //fin ancho de banda

	rate = ancho_banda/tiempo_fin*1E-9;

	/* Rendimiento*/	 
	repeticiones_k=totalAtomLig/nlig;
	repeticiones_i=atoms_l;
	repeticiones_j=atoms_r;
	//dentro_operacion_k= (2)* repeticiones_k;  
	//dentro_operacion_i = 3*repeticiones_i*repeticiones_k;
	dentro_operacion_j= (12)* repeticiones_j * repeticiones_i*repeticiones_k;  
	// fin rendimiento 	 
	

  
	//Rendimiento
	operaciones_f=dentro_operacion_j;

	// lo dividimos entre el tiempo que ha tardado 
	flops = operaciones_f/tiempo_fin*1E-9;

 
    printf("Metricas de Rendimiento y Ancho de banda ,  time %f s, %.2f GB/s, %.2f GFLOPS\n", tiempo_fin, rate,flops);
  
  
  
  printf("Termino electrostatico %f\n", energy[0]);
}



