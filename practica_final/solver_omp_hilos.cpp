#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <sys/time.h>
#include "wtime.h"
#include "definitions.h"
#include "energy_struct.h"
#include "solver.h"

/**
* Funcion que implementa la solvatacion en openmp
*/
extern void forces_OMP_AU (int atoms_r, int atoms_l, int nlig, float *rec_x, float *rec_y, float *rec_z, float *lig_x, float *lig_y, float *lig_z, float *ql ,float *qr, float *energy, int nconformations){

  //printf(" En el fichero solver_omp.cpp se encuentra la funcion forces_omp_au que se debe implementar con la version OpenMP\n");
  float dist, total_elec = 0, miatomo[3], elecTerm;
  int totalAtomLig = nconformations * nlig;

  int k=0;
  int i=0;
  int j=0;
  float tiempo_inicio;
  float tiempo_fin;

  float rate;
  
  float repeticiones_k;
  float total_bucle_k;
float dentro_k;

float repeticiones_i;
float total_bucle_i;
float dentro_i; 

float repeticiones_j;
float total_bucle_j;
float dentro_j_p1;
float dentro_j_p2;
float dentro_j_p3;

float ancho_banda;

float dentro_operacion_k;
float dentro_operacion_i;
float dentro_operacion_j;

float dentro_calcula;

float flops,operaciones_f; 


int numero_hilo;
int numero_hilos;
int numero_hilo_0_k;
int numero_hilo_1_k;
int numero_hilo_2_k;
int numero_hilo_3_k;
int numero_hilo_4_k;
int numero_hilo_5_k;
int numero_hilo_6_k;
int numero_hilo_7_k;
int numero_hilo_8_k;
int numero_hilo_9_k;
int numero_hilo_10_k;
int numero_hilo_11_k;


int numero_hilo_0_i;
int numero_hilo_1_i;
int numero_hilo_2_i;
int numero_hilo_3_i;
int numero_hilo_4_i;
int numero_hilo_5_i;
int numero_hilo_6_i;
int numero_hilo_7_i;
int numero_hilo_8_i;
int numero_hilo_9_i;
int numero_hilo_10_i;
int numero_hilo_11_i;

int numero_hilo_0_j;
int numero_hilo_1_j;
int numero_hilo_2_j;
int numero_hilo_3_j;
int numero_hilo_4_j;
int numero_hilo_5_j;
int numero_hilo_6_j;
int numero_hilo_7_j;
int numero_hilo_8_j;
int numero_hilo_9_j;
int numero_hilo_10_j;
int numero_hilo_11_j;


  //omp_set_num_threads(8);
   //printf(" Numero de procesos:%d\n",omp_get_max_threads());
 
   numero_hilo_0_i=0;
    numero_hilo_1_i=0;
	 numero_hilo_2_i=0;
	  numero_hilo_3_i=0;
	   numero_hilo_4_i=0; 
	   numero_hilo_5_i=0;
	    numero_hilo_6_i=0;
		 numero_hilo_7_i=0;
		  numero_hilo_8_i=0;
		   numero_hilo_9_i=0;
		    numero_hilo_10_i=0;
			numero_hilo_11_i=0;
			
	  numero_hilo_0_k=0;
    numero_hilo_1_k=0;
	 numero_hilo_2_k=0;
	  numero_hilo_3_k=0;
	   numero_hilo_4_k=0; 
	   numero_hilo_5_k=0;
	    numero_hilo_6_k=0;
		 numero_hilo_7_k=0;
		  numero_hilo_8_k=0;
		   numero_hilo_9_k=0;
		    numero_hilo_10_k=0;
			numero_hilo_11_k=0;		
	
 numero_hilo_0_j=0;
    numero_hilo_1_j=0;
	 numero_hilo_2_j=0;
	  numero_hilo_3_j=0;
	   numero_hilo_4_j=0; 
	   numero_hilo_5_j=0;
	    numero_hilo_6_j=0;
		 numero_hilo_7_j=0;
		  numero_hilo_8_j=0;
		   numero_hilo_9_j=0;
		    numero_hilo_10_j=0;
			numero_hilo_11_j=0;	
			
  
  tiempo_inicio=omp_get_wtime();
  #pragma omp parallel for private(i,k,j)  private(elecTerm,dist,miatomo) shared(lig_x,lig_y,lig_z,energy) reduction(+:total_elec)
  for ( k=0; k < totalAtomLig; k+=nlig) {
    for( i=0;i<atoms_l;i++){
      miatomo[0] = *(lig_x + k + i);
      miatomo[1] = *(lig_y + k + i);
      miatomo[2] = *(lig_z + k + i);
	  
	  
	  numero_hilo=omp_get_thread_num();
	 if (numero_hilo  ==  0 )
	 {
	    
     	 numero_hilo_0_i++;
	 }
	  if (numero_hilo  ==  1 )
	 {
	    
     	 numero_hilo_1_i++;
	 }
	  if (numero_hilo  ==  2 )
	 {
	    
     	 numero_hilo_2_i++;
	 }
	  if (numero_hilo  ==  3 )
	 {
	    
     	 numero_hilo_3_i++;
	 }
	  if (numero_hilo  ==  4 )
	 {
	    
     	 numero_hilo_4_i++;
	 }
	  if (numero_hilo  ==  5 )
	 {
	    
     	 numero_hilo_5_i++;
	 }
	  if (numero_hilo  ==  6 )
	 {
	    
     	 numero_hilo_6_i++;
	 }
	  if (numero_hilo  ==  7 )
	 {
	    
     	 numero_hilo_7_i++;
	 }
	  if (numero_hilo  ==  8 )
	 {
	    
     	 numero_hilo_8_i++;
	 }
	  if (numero_hilo  ==  9 )
	 {
	    
     	 numero_hilo_9_i++;
	 }
	 if (numero_hilo  ==  10 )
	 {
	    
     	 numero_hilo_10_i++;
	 }
	 if (numero_hilo  ==  11)
	 {
	    
     	 numero_hilo_11_i++;
	 }
	 
	  
                      
        for( j=0;j<atoms_r;j++){
          elecTerm = 0;
          dist=calculaDistancia (rec_x[j], rec_y[j], rec_z[j], miatomo[0], miatomo[1], miatomo[2]);
          elecTerm = (ql[i]* qr[j]) / dist;
          total_elec += elecTerm;
		  
		  numero_hilo=omp_get_thread_num();
	 if (numero_hilo  ==  0 )
	 {
	    
     	 numero_hilo_0_j++;
	 }
	  if (numero_hilo  ==  1 )
	 {
	    
     	 numero_hilo_1_j++;
	 }
	  if (numero_hilo  ==  2 )
	 {
	    
     	 numero_hilo_2_j++;
	 }
	  if (numero_hilo  ==  3 )
	 {
	    
     	 numero_hilo_3_j++;
	 }
	  if (numero_hilo  ==  4 )
	 {
	    
     	 numero_hilo_4_j++;
	 }
	  if (numero_hilo  ==  5 )
	 {
	    
     	 numero_hilo_5_j++;
	 }
	  if (numero_hilo  ==  6 )
	 {
	    
     	 numero_hilo_6_j++;
	 }
	  if (numero_hilo  ==  7 )
	 {
	    
     	 numero_hilo_7_j++;
	 }
	  if (numero_hilo  ==  8 )
	 {
	    
     	 numero_hilo_8_j++;
	 }
	  if (numero_hilo  ==  9 )
	 {
	    
     	 numero_hilo_9_j++;
	 }
	 if (numero_hilo  ==  10 )
	 {
	    
     	 numero_hilo_10_j++;
	 }
	 if (numero_hilo  ==  11)
	 {
	    
     	 numero_hilo_11_j++;
	 }
		  
		  
        }
     }
     energy[k/nlig] = total_elec;
     total_elec = 0;
	 
	 numero_hilo=omp_get_thread_num();
	 if (numero_hilo  ==  0 )
	 {
	   
     	 numero_hilo_0_k++;
	 }
	  if (numero_hilo  ==  1 )
	 {
	   
     	 numero_hilo_1_k++;
	 }
	  if (numero_hilo  ==  2 )
	 {
	   
     	 numero_hilo_2_k++;
	 }
	  if (numero_hilo  ==  3 )
	 {
	   
     	 numero_hilo_3_k++;
	 }
	  if (numero_hilo  ==  4 )
	 {
	   
     	 numero_hilo_4_k++;
	 }
	  if (numero_hilo  ==  5 )
	 {
	   
     	 numero_hilo_5_k++;
	 }
	  if (numero_hilo  ==  6 )
	 {
	    
     	 numero_hilo_6_k++;
	 }
	  if (numero_hilo  ==  7 )
	 {
	    
     	 numero_hilo_7_k++;
	 }
	  if (numero_hilo  ==  8 )
	 {
	    
     	 numero_hilo_8_k++;
	 }
	  if (numero_hilo  ==  9 )
	 {
	    
     	 numero_hilo_9_k++;
	 }
	 if (numero_hilo  ==  10 )
	 {
	    
     	 numero_hilo_10_k++;
	 }
	 if (numero_hilo  ==  11)
	 {
	    
     	 numero_hilo_11_k++;
	 }
	 
     	 numero_hilos++;
  }
    printf("Repeticiones Bucle k  %d y numero hilos %d",totalAtomLig/nlig,numero_hilos);
	printf("\nEjecuciones  hilo 0 para  %d",numero_hilo_0_k);
	printf("\nEjecuciones  hilo 1 para  %d",numero_hilo_1_k);
	printf("\nEjecuciones  hilo 2 para  %d",numero_hilo_2_k);
	printf("\nEjecuciones  hilo 3 para  %d",numero_hilo_3_k);
	printf("\nEjecuciones  hilo 4 para  %d",numero_hilo_4_k);
	printf("\nEjecuciones  hilo 5 para  %d",numero_hilo_5_k);
	printf("\nEjecuciones  hilo 6 para  %d",numero_hilo_6_k);
	printf("\nEjecuciones  hilo 7 para  %d",numero_hilo_7_k);
	printf("\nEjecuciones  hilo 8 para  %d",numero_hilo_8_k);
	printf("\nEjecuciones  hilo 9 para  %d",numero_hilo_9_k);
	printf("\nEjecuciones  hilo 10 para  %d",numero_hilo_10_k);
	printf("\nEjecuciones  hilo 11 para  %d",numero_hilo_11_k);
	
	
	printf("Repeticiones Bucle para i   %d",atoms_l);
	printf("\nEjecuciones  hilo 0 para  %d",numero_hilo_0_i);
	printf("\nEjecuciones  hilo 1 para  %d",numero_hilo_1_i);
	printf("\nEjecuciones  hilo 2 para  %d",numero_hilo_2_i);
	printf("\nEjecuciones  hilo 3 para  %d",numero_hilo_3_i);
	printf("\nEjecuciones  hilo 4 para  %d",numero_hilo_4_i);
	printf("\nEjecuciones  hilo 5 para  %d",numero_hilo_5_i);
	printf("\nEjecuciones  hilo 6 para  %d",numero_hilo_6_i);
	printf("\nEjecuciones  hilo 7 para  %d",numero_hilo_7_i);
	printf("\nEjecuciones  hilo 8 para  %d",numero_hilo_8_i);
	printf("\nEjecuciones  hilo 9 para  %d",numero_hilo_9_i);
	printf("\nEjecuciones  hilo 10 para  %d",numero_hilo_10_i);
	printf("\nEjecuciones  hilo 11 para  %d",numero_hilo_11_i);
	
	
	printf("Repeticiones Bucle para j   %d",atoms_r);
	printf("\nEjecuciones  hilo 0 para  %d",numero_hilo_0_j);
	printf("\nEjecuciones  hilo 1 para  %d",numero_hilo_1_j);
	printf("\nEjecuciones  hilo 2 para  %d",numero_hilo_2_j);
	printf("\nEjecuciones  hilo 3 para  %d",numero_hilo_3_j);
	printf("\nEjecuciones  hilo 4 para  %d",numero_hilo_4_j);
	printf("\nEjecuciones  hilo 5 para  %d",numero_hilo_5_j);
	printf("\nEjecuciones  hilo 6 para  %d",numero_hilo_6_j);
	printf("\nEjecuciones  hilo 7 para  %d",numero_hilo_7_j);
	printf("\nEjecuciones  hilo 8 para  %d",numero_hilo_8_j);
	printf("\nEjecuciones  hilo 9 para  %d",numero_hilo_9_j);
	printf("\nEjecuciones  hilo 10 para  %d",numero_hilo_10_j);
	printf("\nEjecuciones  hilo 11 para  %d",numero_hilo_11_j);
	
	
	
	tiempo_fin=omp_get_wtime()-tiempo_inicio;
    
	
	/* ancho de banda*/
	repeticiones_k=totalAtomLig/nlig;
	total_bucle_k= 1 + (2 * repeticiones_k) + (3 * repeticiones_k);      
	repeticiones_i=atoms_l;
	total_bucle_i= ((1 + (2 * repeticiones_i) + (2 * repeticiones_i))*repeticiones_k ); 		
	dentro_i = (6*3 + 5*3) *repeticiones_i*repeticiones_k;
	repeticiones_j=atoms_r;
	total_bucle_j= ((1 + (2 * repeticiones_j) + (2 * repeticiones_j))*repeticiones_i*repeticiones_k ); 		
	// cambiado de 13
	dentro_j_p1= (38)* repeticiones_j * repeticiones_i*repeticiones_k ; 
	dentro_calcula=(6*3*4 +  4*4 + 4) * repeticiones_j * repeticiones_i*repeticiones_k;     
	dentro_j_p2= (4+10)* repeticiones_j * repeticiones_i*repeticiones_k;  
	dentro_j_p3= (4+8)* repeticiones_j * repeticiones_i*repeticiones_k; 
	dentro_k= (6+8)* repeticiones_k;
	
    ancho_banda=	 total_bucle_k + dentro_k +  total_bucle_i + dentro_i +  total_bucle_j + dentro_j_p1 + dentro_j_p2 + dentro_j_p3 + dentro_calcula;
     //fin ancho de banda

	rate = ancho_banda/tiempo_fin*1E-9;

	/* Rendimiento*/	 
	repeticiones_k=totalAtomLig/nlig;
	repeticiones_i=atoms_l;
	repeticiones_j=atoms_r;
	dentro_operacion_k= (2)* repeticiones_k;  
	dentro_operacion_i = 3*repeticiones_i*repeticiones_k;
	dentro_operacion_j= (23)* repeticiones_j * repeticiones_i*repeticiones_k;  
	// fin rendimiento 	 
	

  
	//Rendimiento
	operaciones_f=dentro_operacion_k + dentro_operacion_i+ dentro_operacion_j;

	// lo dividimos entre el tiempo que ha tardado 
	flops = operaciones_f/tiempo_fin*1E-9;

 
    printf("Metricas de Rendimiento y Ancho de banda ,  time %f s, %.2f GB/s, %.2f GFLOPS\n", tiempo_fin, rate,flops);
  
  
  
  printf("Termino electrostatico %f\n", energy[0]);
}



