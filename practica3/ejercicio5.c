/* CHM  03/11/2019 */
/* reduce_Vectores  */
/* practica 3*/
/* Ejercicio 5*/
/*Reduce Vector*/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define FIL  10241024


int i,j,pos;

void inicializa_Vectores(float * M_A)
{
	
	 for(i=0;i<FIL;i++)
    {
			pos=i; 
            M_A[i]=1.0f; // valores para A
		
        
    }
	
}

float reduce_Vectores(float * M_A)
{
	float suma=0;
	 #pragma omp parallel for  private(i) shared(M_A) reduction(+:suma) 
	 for(i=0;i<FIL;i++)
    {
        suma +=M_A[i];
        
    }
	return suma;
}

float sin_reduce_Vectores(float * M_A)
{
	float suma=0;
	 for(i=0;i<FIL;i++)
    {
        suma +=M_A[i];
        
    }
	return suma;
}

	

int main(void)
{
float resultado_suma;	  
								
float    * M_A;

    printf("Programa que reduce vectores:\n\n"); 
	printf("Reservando memoria :\n\n");

   
	   
	M_A = (float *) malloc(FIL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	

    inicializa_Vectores(M_A);      
    omp_set_num_threads(2); 
	double empieza=omp_get_wtime();
    resultado_suma=reduce_Vectores(M_A);  
    double final=omp_get_wtime();  
	double tiempo_2_pro=final-empieza;
	printf("Resultado para 2 procesos %f:\n",resultado_suma);
	
	
	omp_set_num_threads(4); 	

    inicializa_Vectores(     M_A);    	
	empieza=omp_get_wtime();
    resultado_suma=reduce_Vectores(M_A);  
    final=omp_get_wtime();  
	double tiempo_4_pro=final-empieza;	
	printf("Resultado para 4 procesos %f:\n",resultado_suma);
	
	
	omp_set_num_threads(6); 

    inicializa_Vectores(      M_A);    	
	empieza=omp_get_wtime();
    resultado_suma=reduce_Vectores( M_A);  
    final=omp_get_wtime();  	
	double tiempo_6_pro=final-empieza;
	printf("Resultado para 6 procesos %f:\n",resultado_suma);
	
	
    omp_set_num_threads(8); 

    inicializa_Vectores(  M_A);    	
	empieza=omp_get_wtime();
    resultado_suma=reduce_Vectores(  M_A);  
    final=omp_get_wtime();  	
	double tiempo_8_pro=final-empieza;
	printf("Resultado para 8 procesos %f:\n",resultado_suma);
	
	
	omp_set_num_threads(1); 
	
    inicializa_Vectores(     M_A);    	
	empieza=omp_get_wtime();
    resultado_suma=sin_reduce_Vectores(  M_A);  
    final=omp_get_wtime();  	
	double tiempo_sin_pro=final-empieza;
	printf("Resultado sin paralelizar %f:\n",resultado_suma);
	
	//imprime_Vectores( M_Res);
	printf("Tiempo sin paralelizar %fs :\n",tiempo_sin_pro);
	printf("Tiempo 2 procesos %fs :\n",tiempo_2_pro);
	printf("Tiempo 4 procesos %fs :\n",tiempo_4_pro);
	printf("Tiempo 6 procesos %fs :\n",tiempo_6_pro);
	printf("Tiempo 8 procesos %fs :\n",tiempo_8_pro);
	
	//liberamos memoria
	free(M_A);
	
    return 0;
}

