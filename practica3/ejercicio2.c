/* CHM  30/10/2019 */
/* Practica 3 */
/* Ejercicio 2  */
#include <stdio.h>
#include <omp.h>
#define N  1000

int i,x,y,procesos;

void main(void)
{
x=0;	  
y=1;
printf("Programa Ejercicio 2 , practica 3 \n"); 								
printf("Procesos:"); 								
scanf("%d", &procesos);

omp_set_num_threads(procesos);

x=0;
y=1;
for (i=0;i<N;i++)
        {
         x=i+y;  
    	 //printf("Numero proceso %d de %d iteracion %d \n",omp_get_thread_num(),omp_get_num_threads(),i); 				}				
 
}	 
printf("Valor de x  secuencial %d \n",x);  


#pragma omp parallel for  private(i) 
 for (i=0;i<N;i++)
        x=i+y;  
    	 //printf("Numero proceso %d de %d iteracion %d \n",omp_get_thread_num(),omp_get_num_threads(),i); 				}				

	
 printf("Valor de x  paralelo y %d \n",x); 
x=0;
y=1;
#pragma omp parallel for lastprivate(x) 
 for (i=0;i<N;i++)
        {
         x=i+y;  
    	 //printf("Numero proceso %d de %d iteracion %d \n",omp_get_thread_num(),omp_get_num_threads(),i); 				}				
 
}	
 printf("Valor de x  paralelo lastprivate x %d \n",x); 
 
 x=0;
 y=1;
#pragma omp parallel for shared(x,y)
 for (i=0;i<N;i++)
        {
         x=i+y;  
    	 //printf("Numero proceso %d de %d iteracion %d \n",omp_get_thread_num(),omp_get_num_threads(),i); 				}				
 
}	
 printf("Valor de x  paralelo shared x,y %d \n",x); 
 
 
  x=0;
 y=1;
#pragma omp parallel for firstprivate(y)
 for (i=0;i<N;i++)
        {
         x=i+y;  
    	 //printf("Numero proceso %d de %d iteracion %d \n",omp_get_thread_num(),omp_get_num_threads(),i); 				}				
 
}	
 printf("Valor de x  paralelo first private(y) %d \n",x); 



 
}

