﻿/* CHM  08/11/2019 */
/* multiplicar matrices   */
/* Practica 3*/
/* Parte 3 */
/* Ejercicio 2*/
/* multiplicar matrices */

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define FIL  2048
#define COL  2048 /*tamaño maximo*/
#define DIMENSION 2048

int i,j,k;

void inicializa_matrices(float   * M_Res,float * M_A,float * M_B)
{
	
	 for(i=0;i<FIL;i++)
    {
        for(j=0;j<COL;j++)				
        {
			int pos=i*COL+j;
			
			if (pos%2 == 0) 		  
			{		
               M_A[pos]=1.3f; // valores para A
		       M_B[pos]=1.4f; // valores para B
			}   
			else
			{	
			   M_A[pos]=1.7f; // valores para A
               M_B[pos]=1.6f; // valores para A		   
		    } 
		   
			M_Res[pos]=0.0; 
        }
    }
	
}


void multiplica_matrices_secuencial(float   * M_Res,float * M_A,float * M_B)
{
	   	
	    
       for(i=0;i<DIMENSION;i++)        
         for(j=0;j<DIMENSION;j++)
          {        
	        float sum=0.0;			
			  for(k=0;k<DIMENSION;k++)
				  sum+=M_A[i*DIMENSION+k]*M_B[k*DIMENSION+j];
		        
		   M_Res[i*DIMENSION+j]=sum;
							
         }
	
}
void multiplica_matrices_primera(float   * M_Res,float * M_A,float * M_B)
{
	   	
	   omp_set_nested(0); 
       #pragma omp parallel for collapse(2)
       for(i=0;i<DIMENSION;i++)        
         for(j=0;j<DIMENSION;j++)
          {        
	        float sum=0.0;
			//int pos=i*DIMENSION+j;
			  #pragma omp parallel for reduction(+:sum) 
			  for(k=0;k<DIMENSION;k++)
				  sum+=M_A[i*DIMENSION+k]*M_B[k*DIMENSION+j];
		        
		   M_Res[i*DIMENSION+j]=sum;
							
         }
	
}


void multiplica_matrices_segunda(float   * M_Res,float * M_A,float * M_B)
{
	   	
	   omp_set_nested(0); 
    #pragma omp parallel  for  private(k,j)
       for(i=0;i<DIMENSION;i++)        
         for(j=0;j<DIMENSION;j++)
          {        
	        float sum=0.0;			
			  for(k=0;k<DIMENSION;k++)
				  sum+=M_A[i*DIMENSION+k]*M_B[k*DIMENSION+j];
		        
		   M_Res[i*DIMENSION+j]=sum;
							
         }
	
}

/*esta la mejor */
void multiplica_matrices_tercera(float   * M_Res,float * M_A,float * M_B)
{
	   	
  float sum=0.0;
	   
    #pragma omp parallel  for  private(k,j) reduction(+:sum)
       for(i=0;i<DIMENSION;i++)        
         for(j=0;j<DIMENSION;j++)
          {        
	         sum=0.0;			
			  for(k=0;k<DIMENSION;k++)
				  sum+=M_A[i*DIMENSION+k]+M_B[k*DIMENSION+j];
		        
		   M_Res[i*DIMENSION+j]=sum;
							
         }
	
}



void imprime_matrices(float   * M_Res)
{
    /* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(i=0;i<FIL;i++)
    {
        for(j=0;j<COL;j++) {
			int pos=i*COL+j;
			 printf("%10f",M_Res[pos]);
		}
        printf("\n");
    }
}
void comprueba_matrices(float   * M_Res_sec,float   * M_Res_otra)
{
    int iguales=0;
	int iguales_otra=0;
	
	/* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(i=0;i<FIL;i++)
           for(j=0;j<COL;j++)
			{
			int pos=i*FIL+j;						 
			  if (M_Res_sec[pos] != M_Res_otra[pos])  
			  {
                  // printf("\n\nLa matriz resultado  y otra no seon iguales:\n\n");
				   iguales_otra+=1;
			  }
			}  
			
			if (iguales_otra == 0)
     		{
				printf("Solucion correcta\n");
				printf("\n\nSecuencial %f  y Paralelo otra %f:\n\n",M_Res_sec[101],M_Res_otra[101]);
			}	
		
			else  {
				 printf("***************ERRROR*************************\n");
				 printf("\n\nSecuenciañ %f  y Paralelo otra %f:\n\n",M_Res_sec[0],M_Res_otra[0]);
			}		   
}		


int main(void)
{
	  
								
float   * M_Res, * M_A, * M_B;
float   * M_Res_ocho;
float   * M_Res_otro;
float   * M_aux;
	
double final,empieza;

    printf("Programa Multiplica matrices :\n\n"); 
	printf("Reservando memoria :\n\n");
	
	M_Res_ocho= (float *) malloc(sizeof(float)*FIL*COL);
    if (M_Res_ocho == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	M_Res_otro= (float *) malloc(sizeof(float)*FIL*COL);
    if (M_Res_otro == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }

    M_Res = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_Res == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	   
	M_A = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	M_B = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_B	== NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
  
	
	//secuencial      
	omp_set_num_threads(1);		
	printf("Metodo multiplica reducion 1 :\n\n");
    inicializa_matrices(M_Res,M_A,M_B);      	    
	empieza=omp_get_wtime();		
    multiplica_matrices_secuencial(M_Res,  M_A,M_B);  
	final=omp_get_wtime();  
	double tiempo_pro=final-empieza;
	
	//primera planificacion
	omp_set_num_threads(8);		
	printf("Metodo multiplica reducion 8 :\n\n");
    inicializa_matrices(M_Res_ocho,M_A,M_B);      	    
	empieza=omp_get_wtime();		
    multiplica_matrices_primera(M_Res_ocho,  M_A,M_B);  
	final=omp_get_wtime();  
	double tiempo_8_pro=final-empieza;
	comprueba_matrices(M_Res,M_Res_ocho);
	
	//segunda planificacion
	omp_set_num_threads(8);		
	printf("Metodo multiplica reducion 8 otra planificacion :\n\n");
    inicializa_matrices(M_Res_otro,M_A,M_B);      	    
	empieza=omp_get_wtime();		
    multiplica_matrices_segunda(M_Res_otro,  M_A,M_B);  	
	final=omp_get_wtime();  
	double tiempo_6_pro=final-empieza;	
	comprueba_matrices(M_Res,M_Res_otro);  

//tercera planificacion
	omp_set_num_threads(8);		
	printf("Metodo multiplica reducion 8 otra planificacion :\n\n");
    inicializa_matrices(M_Res_ocho,M_A,M_B);      	    
	empieza=omp_get_wtime();		
    multiplica_matrices_tercera(M_Res_ocho,  M_A,M_B);  	
	final=omp_get_wtime();  
	double tiempo_4_pro=final-empieza;	
	comprueba_matrices(M_Res,M_Res_ocho);    	
	
	//tiempos
	printf("\nTiempo planificacion  secuencial 1 proceso %fs resultado \n",tiempo_pro);
	printf("Tiempo planificacion primera  8 procesos %fs resultado \n",tiempo_8_pro);	
    printf("Tiempo planificacion segunda 8 procesos %fs resultado \n",tiempo_6_pro);
	printf("Tiempo planificacion tercera 8 procesos %fs resultado \n",tiempo_4_pro);
	
	//liberamos memoria*/
	free(M_A);
	free(M_B);
	free(M_Res);
	free(M_Res_ocho);
	free(M_Res_otro);
	
    return 0;
}

