/* CHM  20/10/2019 */
/* Suma_vectores  */
/* practica 3*/
/* Ejercicio 3*/
/*SUMA Vectores */

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define FIL  4096


int i,j,pos;

void inicializa_matrices(float   * M_Res,float * M_A, float * M_B)
{
	
	 for(i=0;i<FIL;i++)
    {
			pos=i; 
            M_A[i]=1.0f; // valores para A
			M_B[i]=1.0f; // valores para B
		    M_Res[i]=0; 
        
    }
	
}

void suma_matrices(float   * M_Res,float * M_A,  float * M_B)
{
	 #pragma omp parallel for  private(i) shared(M_A,M_B,M_Res) 
	 for(i=0;i<FIL;i++)
    {
        M_Res[i]=M_A[i]+M_B[i]; 
        
    }
	
}

void sin_suma_matrices(float   * M_Res,float * M_A,  float * M_B)
{
	
	 for(i=0;i<FIL;i++)
    {
        M_Res[i]=M_A[i]+M_B[i]; 
        
    }
	
}

void imprime_matrices(float   * M_Res)
{
    /* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(i=0;i<FIL;i++)
    {      
			 printf("%10f",M_Res[pos]);	 
    }
}
	

int main(void)
{
	  
								
float   * M_Res, * M_A, * M_B;

    printf("Programa que suma vectores:\n\n"); 
	printf("Reservando memoria :\n\n");

    M_Res = (float *) malloc(FIL*sizeof(float));
	
	if (M_Res == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	   
	M_A = (float *) malloc(FIL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	M_B = (float *) malloc(FIL*sizeof(float));
	
	if (M_B == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	printf("Dando valores a las matrices :\n\n");
    inicializa_matrices(    M_Res,  M_A,   M_B);      
     
    printf("Sumando matrices :\n\n");
	
	omp_set_num_threads(2); 
	double empieza=omp_get_wtime();
    suma_matrices(M_Res,  M_A, M_B);  
    double final=omp_get_wtime();  
	double tiempo_2_pro=final-empieza;
	
	
	omp_set_num_threads(4); 	
	printf("Dando valores a las matrices para 4 procesos :\n\n");
    inicializa_matrices(    M_Res,  M_A,   M_B);    	
	empieza=omp_get_wtime();
    suma_matrices(M_Res,  M_A, M_B);  
    final=omp_get_wtime();  
	double tiempo_4_pro=final-empieza;
	
	
	omp_set_num_threads(6); 
	printf("Dando valores a las matrices para 6 procesos :\n\n");
    inicializa_matrices(    M_Res,  M_A,   M_B);    	
	empieza=omp_get_wtime();
    suma_matrices(M_Res,  M_A, M_B);  
    final=omp_get_wtime();  	
	double tiempo_6_pro=final-empieza;
	
	
    omp_set_num_threads(8); 
	printf("Dando valores a las matrices para 8 procesos :\n\n");
    inicializa_matrices(    M_Res,  M_A,   M_B);    	
	empieza=omp_get_wtime();
    suma_matrices(M_Res,  M_A, M_B);  
    final=omp_get_wtime();  	
	double tiempo_8_pro=final-empieza;
	
	omp_set_num_threads(8); 
	printf("Dando valores a las matrices para 8 procesos :\n\n");
    inicializa_matrices(    M_Res,  M_A,   M_B);    	
	empieza=omp_get_wtime();
    sin_suma_matrices(M_Res,  M_A, M_B);  
    final=omp_get_wtime();  	
	double tiempo_sin_pro=final-empieza;
	
	//imprime_matrices( M_Res);
	printf("Tiempo sin paralelizar %fs :\n",tiempo_sin_pro);
	printf("Tiempo 2 procesos %fs :\n",tiempo_2_pro);
	printf("Tiempo 4 procesos %fs :\n",tiempo_4_pro);
	printf("Tiempo 6 procesos %fs :\n",tiempo_6_pro);
	printf("Tiempo 8 procesos %fs :\n",tiempo_8_pro);
	
	//liberamos memoria
	free(M_A);
	free(M_B);
	free(M_Res);
	
    return 0;
}

