﻿/* CHM  08/11/2019 */
/* METODO JACOBI  */
/* Practica 3*/
/* Parte 3 */
/* Ejercicio 1*/
/* Paralelizar JAcobi*/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define FIL  1024
#define COL  1024 /*tamaño maximo*/
#define NUM_ITER 1000//000

int i,j,k;

void inicializa_matrices(float   * M_Res,float * M_A)
{
	
	 for(i=0;i<FIL;i++)
    {
        for(j=0;j<COL;j++)				
        {
			int pos=i*COL+j; 
			if (pos%2 == 0) 
               M_A[pos]=111175.0f; // valores para A
			else
			   M_A[pos]=11150.0f; // valores para A	
			//M_Res[pos]=12.0; 
        }
    }
	
}

void metodo_jacobi_secuencial(float   * M_Res,float * M_A)
{
	

       //#pragma omp parallel for  collapse(2) shared(M_Res,M_A)
       for(i=1;i<FIL-1;i++)        
         for(j=1;j<COL-1;j++)
          {        
			int pos=i*COL+j;
			
			int posI_ant=(i-1)*COL+j;
			int posI_pos=(i+1)*COL+j;
			int posJ_ant=i*COL+(j+1);
			int posJ_pos=i*COL+(j-1);
			
		    M_Res[pos]=0.3*(M_A[pos]+M_A[posI_ant]
			                        +M_A[posI_pos]
									+M_A[posJ_ant]
                                    +M_A[posJ_pos]
							); 
         }
		 
		 
		 
			
}

void metodo_jacobi_paralelo(float   * M_Res,float * M_A)
{
	

       #pragma omp parallel for  collapse(2) shared(M_Res,M_A) // probar....
       for(i=1;i<FIL-1;i++)        
         for(j=1;j<COL-1;j++)
          {        
			int pos=i*COL+j;
			
			int posI_ant=(i-1)*COL+j;
			int posI_pos=(i+1)*COL+j;
			int posJ_ant=i*COL+(j+1);
			int posJ_pos=i*COL+(j-1);
			
		    M_Res[pos]=0.3*(M_A[pos]+M_A[posI_ant]
			                        +M_A[posI_pos]
									+M_A[posJ_ant]
                                    +M_A[posJ_pos]
							); 
         }		 	

}

void imprime_matrices(float   * M_Res)
{
    /* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(i=0;i<FIL;i++)
    {
        for(j=0;j<COL;j++) {
			int pos=i*COL+j;
			 printf("%10f",M_Res[pos]);
		}
        printf("\n");
    }
}

void comprueba_matrices(float   * M_Res_sec,float   * M_Res_otra)
{
    int iguales=0;
	int iguales_otra=0;
	
	/* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(i=0;i<FIL;i++)
           for(j=0;j<COL;j++)
			{
			int pos=i*FIL+j;						 
			  if (M_Res_sec[pos] != M_Res_otra[pos])  
			  {
                  // printf("\n\nLa matriz resultado  y otra no seon iguales:\n\n");
				   iguales_otra+=1;
			  }
			}  
			
			if (iguales_otra == 0)
     		{
				printf("Solucion correcta\n");
				printf("\n\nSecuenciañ %f  y Paralelo otra %f:\n\n",M_Res_sec[5],M_Res_otra[5]);
				
			}	
		
			else  {
				printf("*****ERROR*********\n");
				 printf("\n\nSecuenciañ %f  y Paralelo otra %f:\n\n",M_Res_sec[0],M_Res_otra[0]);
			}		   
}		

int main(void)
{
	  
								
float   * M_Res, * M_A, * M_B;
float   * M_Res_para;

float   * M_aux;
	
double final,empieza;

    printf("Programa JACOBI :\n\n"); 
	printf("Reservando memoria :\n\n");
	
    M_Res_para = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_Res_para == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }


    M_Res = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_Res == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	   
	M_A = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
		   
	
	M_aux=NULL;
	
    omp_set_num_threads(1);	
	printf("Dando valores a las matrices :\n\n");
    inicializa_matrices(M_Res,M_A);      	
    printf("Metodo JACOBI :\n\n");
	empieza=omp_get_wtime();
	for(k=0;k<NUM_ITER ;k++)
    {
		
    metodo_jacobi_secuencial(M_Res,  M_A);  
	 
	    M_aux=M_A;
		M_A=M_Res;
		M_Res=M_aux;
	    	
    }
	final=omp_get_wtime();  
	double tiempo_pro=final-empieza;
    //printf("Muestra REsultado matrices :\n\n");
	//imprime_matrices(M_Res);  
	
	M_aux=NULL;
    omp_set_num_threads(8);	
	printf("Dando valores a las matrices 8  :\n");
    inicializa_matrices(M_Res_para,M_A);      	    
	empieza=omp_get_wtime();
    	for(k=0;k<NUM_ITER ;k++)
    {
		
    metodo_jacobi_paralelo(M_Res_para,  M_A);  
	    M_aux=M_A;
		M_A=M_Res_para;
		M_Res_para=M_A;
	    	
    } 
	final=omp_get_wtime();  
	double tiempo_8_pro=final-empieza;
	comprueba_matrices(M_Res,M_Res_para);   
  //  printf("Muestra REsultado matrices :\n\n");
	//imprime_matrices(M_Res);  
	
    
	
	
	printf("\n Tiempo planificacion 1 procesos %fs resultado \n",tiempo_pro);
	printf("Tiempo planificacion 8procesos %fs resultado \n",tiempo_8_pro);
	
    
	
	//liberamos memoria
	free(M_A);
	free(M_Res);
	//free(M_aux);
	free(M_Res_para);
	
    return 0;
}

