/* CHM  03/11/2019 */
/* reduce_Vectores  */
/* Practica 3*/
/* Parte 1   */
/* Ejercicio 5*/
/*Reduce Vector*/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define FIL  1024102411


int i,j,pos;

void inicializa_Vectores(float * M_A)
{
	
	 for(i=0;i<FIL;i++)
    {
			pos=i; 
            M_A[i]=2.1f; // valores para A
		
        
    }
	
}

float reduce_Vectores(float * M_A)
{
	float suma=0;
	 #pragma omp parallel for  private(i,suma) shared(M_A) 
	 for(i=0;i<FIL;i++)
    {
		#pragma omp critical
        suma +=M_A[i];
        
    }
	return suma;
}

float sin_reduce_Vectores(float * M_A)
{
	float suma=0;
	 for(i=0;i<FIL;i++)
    {
        suma +=M_A[i];
        
    }
	return suma;
}


	

int main(void)
{
float resultado_suma;	
float final,empieza;  
								
float    * M_A;

    printf("Programa que reduce vectores:\n\n"); 
	printf("Reservando memoria :\n\n");

   
	   
	M_A = (float *) malloc(FIL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	

	
	

	
	
    omp_set_num_threads(8); 

    inicializa_Vectores(  M_A);    	
	empieza=omp_get_wtime();
    resultado_suma=sin_reduce_Vectores(  M_A);  
    final=omp_get_wtime();  	
	double tiempo_8_pro=final-empieza;
	printf("Resultado para 8 procesos %f:\n",resultado_suma);
	
	
	omp_set_num_threads(1); 
	
    inicializa_Vectores(     M_A);    	
	empieza=omp_get_wtime();
    resultado_suma=sin_reduce_Vectores(  M_A);  
    final=omp_get_wtime();  	
	double tiempo_sin_pro=final-empieza;
	printf("Resultado sin paralelizar %f:\n",resultado_suma);
	
	//imprime_Vectores( M_Res);
	printf("Tiempo sin paralelizar %fs :\n",tiempo_sin_pro);	
	printf("Tiempo 8 procesos %fs :\n",tiempo_8_pro);
	
	//liberamos memoria
	free(M_A);
	
    return 0;
}

