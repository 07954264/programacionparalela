/* CHM  30/10/2019 */
/* Practica 3 */
/* Ejercicio 1  */
#include <stdio.h>
#include <omp.h>
#define N  13

int i,x,procesos;

int main(void)
{
x=0;	  
printf("Programa Ejercicio 1 , practica 3"); 								
printf("Procesos:"); 								
scanf("%d", &procesos);
omp_set_num_threads(procesos);

#pragma omp parallel
{
#pragma omp for private(i)
 for (i=0;i<N;i++)
        {
         x=x+i;  
    	 printf("Numero proceso %d de %d iteracion %d \n",omp_get_thread_num(),omp_get_num_threads(),i); 				}				
 
}	
    return 0;
}

