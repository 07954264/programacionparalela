﻿/* CHM  08/11/2019 */
/* SENCTIL   */
/* Practica 3*/
/* Ejercicio 3*/
/* SENCTIL 2D */

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define FIL  4096
#define COL  4096 /*tamaño maximo*/
#define DIMENSION 100

int i,j,k;

void inicializa_matrices(float   * M_Res,float * M_A,float * M_B)
{
	
	 for(i=0;i<FIL;i++)
    {
        for(j=0;j<COL;j++)				
        {
			int pos=i*COL+j;
			
			if (pos%2 == 0) 		  
			{		
               M_A[pos]=1.3f; // valores para A
		       M_B[pos]=1.7f; // valores para B
			}   
			else
			{	
			   M_A[pos]=1.5f; // valores para A
               M_B[pos]=1.2f; // valores para A		   
		    } 
		   
			M_Res[pos]=0.0; 
        }
    }
	
}

void stencil_secuencial(float   * M_Res,float * M_A,float * M_B)
{
	
	float   * M_aux;
	 	   
	  for(k=0;k<DIMENSION;k++) 
	  {	  
      
       for(i=1;i<FIL-1;i++)        
         for(j=1;j<COL-1;j++)           
	         M_Res[i*COL+j]=M_A[(i-1)*COL+j]+M_A[(i+1)*COL+j]+M_A[i*COL+j-1]+  M_A[i*COL+j+1] ;
	     
		    M_aux=M_Res;
		    M_Res=M_A;
		    M_A=M_aux;
	  }						        		 	
}

void stencil_paralelo(float   * M_Res,float * M_A,float * M_B)
{
	
	float   * M_aux;
	 	   
	  for(k=0;k<DIMENSION;k++) 
	  {	  
       #pragma omp parallel for collapse(2) private(i,j)
       for(i=1;i<FIL-1;i++)        
         for(j=1;j<COL-1;j++)           
	         M_Res[i*COL+j]=M_A[(i-1)*COL+j]+M_A[(i+1)*COL+j]+M_A[i*COL+j-1]+  M_A[i*COL+j+1] ;
	     
		    M_aux=M_Res;
		    M_Res=M_A;
		    M_A=M_aux;
	  }						        		 	
}

void comprueba_matrices(float   * M_Res_sec,float   * M_Res_otra)
{
    int iguales=0;
	int iguales_otra=0;
	
	/* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(i=0;i<FIL;i++)
           for(j=0;j<COL;j++)
			{
			int pos=i*FIL+j;						 
			  if (M_Res_sec[pos] != M_Res_otra[pos])  
			  {
                  // printf("\n\nLa matriz resultado  y otra no seon iguales:\n\n");
				   iguales_otra+=1;
			  }
			}  
			
			if (iguales_otra == 0)
     		{
				printf("Solucion correcta");
				
			}	
		
			else  {
				 printf("\n\nDiferencias %i  y Diferencias otra %i:\n\n",iguales,iguales_otra);
				 printf("\n\nSecuenciañ %f  y Paralelo otra %f:\n\n",M_Res_sec[0],M_Res_otra[0]);
			}		   
}		


void imprime_matrices(float   * M_Res)
{
    /* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(i=0;i<FIL;i++)
    {
        for(j=0;j<COL;j++) {
			int pos=i*COL+j;
			 printf("%10f",M_Res[pos]);
		}
        printf("\n");
    }
}
	

int main(void)
{
	  
								
float   * M_Res, * M_A, * M_B;
 float   * M_aux;
 float   * M_Res_otro;
	
double final,empieza;

    printf("Programa Multiplica matrices :\n\n"); 
	printf("Reservando memoria :\n\n");
    
    M_Res_otro= (float *) malloc(sizeof(float)*FIL*COL);
    if (M_Res_otro == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }	
	   
    M_Res = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_Res == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	   
	M_A = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	M_B = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_B	== NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
    omp_set_num_threads(1);	
	printf("Dando valores a las matrices :\n\n");
    inicializa_matrices(M_Res,M_A,M_B);      	
    printf("Metodo senticl secuencial:\n\n");
	empieza=omp_get_wtime();		
    stencil_secuencial(M_Res,  M_A,M_B);  
	final=omp_get_wtime();  
	double tiempo_pro=final-empieza;
	//imprime_matrices(M_Res);  
	
	
	omp_set_num_threads(2);		
	printf("Metodo senticl reducion2 :\n\n");
    inicializa_matrices(M_Res_otro,M_A,M_B);      	    
	empieza=omp_get_wtime();		
    stencil_paralelo(M_Res_otro,  M_A,M_B);  
	final=omp_get_wtime();  
	double tiempo_2_pro=final-empieza;
	//imprime_matrices(M_Res);  
	comprueba_matrices(M_Res,M_Res_otro);  
	
	omp_set_num_threads(4);		
	printf("Metodo setncil reducion 4:\n\n");
    inicializa_matrices(M_Res_otro,M_A,M_B);      	    
	empieza=omp_get_wtime();		
    stencil_paralelo(M_Res_otro,  M_A,M_B);  
	final=omp_get_wtime();  
	double tiempo_4_pro=final-empieza;
	comprueba_matrices(M_Res,M_Res_otro);  
	
	omp_set_num_threads(6);		
	printf("Metodo multiplica reducion 6 :\n\n");
    inicializa_matrices(M_Res_otro,M_A,M_B);      	    
	empieza=omp_get_wtime();		
    stencil_paralelo(M_Res_otro,  M_A,M_B);  
	final=omp_get_wtime();  
	double tiempo_6_pro=final-empieza;
	comprueba_matrices(M_Res,M_Res_otro);  
    
	omp_set_num_threads(8);		
	printf("Metodo multiplica reducion 8 :\n\n");
    inicializa_matrices(M_Res_otro,M_A,M_B);      	    
	empieza=omp_get_wtime();		
    stencil_paralelo(M_Res_otro,  M_A,M_B);  
	final=omp_get_wtime();  
	double tiempo_8_pro=final-empieza;
	comprueba_matrices(M_Res,M_Res_otro);  
	
	printf("\n Tiempo planificacion 1 procesos %fs resultado \n",tiempo_pro);
	printf("Tiempo planificacion  2 procesos %fs resultado \n",tiempo_2_pro);
	printf("\n Tiempo planificacion 4 procesos %fs resultado \n",tiempo_4_pro);
	printf("Tiempo planificacion  6 procesos %fs resultado \n",tiempo_6_pro);		
	printf("Tiempo planificacion  8 procesos %fs resultado \n",tiempo_8_pro);
    
	
	//liberamos memoria*/
	free(M_A);
	free(M_B);
	free(M_Res);
	
    return 0;
}

