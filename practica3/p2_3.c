/* CHM  20/10/2019 */
/* multiplica_vectores  */
/* practica 3*/
/* Ejercicio 3*/
/*multiplica Vectores */

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define FIL  10000000


int i,j,pos;

void inicializa_matrices(float * M_A, float * M_B)
{
	
	 for(i=0;i<FIL;i++)
    {
			
            M_A[i]=1.0f; // valores para A
			M_B[i]=1.0f; // valores para B
		    
        
    }
	
}


float multiplica_matrices(float * M_A,  float * M_B)
{
	float total;
	total=0;
	 #pragma omp parallel for  private(i) shared(M_A,M_B) 
	 for(i=0;i<FIL;i++)
  
       #pragma omp critical
        total+=M_A[i]*M_B[i]; 
		        
  
	return total;
}

float sin_multiplica_matrices(float * M_A,  float * M_B)
{
	float total;
	total=0;
	 for(i=0;i<FIL;i++)
    {
        total=total+M_A[i]*M_B[i]; 
		        
    }
	return total;
}


	

int main(void)
{
	  
								
float    * M_A, * M_B;
double final,empieza;
float total;
float suma1,suma2,suma4,suma6,suma8;
    printf("Programa que multiplica vectores:\n\n"); 
	printf("Reservando memoria :\n\n");

	   
	M_A = (float *) malloc(FIL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	M_B = (float *) malloc(FIL*sizeof(float));
	
	if (M_B == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	 
    printf("Prodcuto escalar :\n\n");
	
		
	printf("Dando valores a las matrices para 1 procesos  :\n\n");
    inicializa_matrices(  M_A,   M_B);    	
	empieza=omp_get_wtime();
    suma1=sin_multiplica_matrices(M_A, M_B);  
    final=omp_get_wtime();  
	double tiempo_pro=final-empieza;
	
	
	
	omp_set_num_threads(2); 	
	printf("Dando valores a las matrices para 2 procesos  :\n\n");
   // inicializa_matrices(  M_A,   M_B);    	
	empieza=omp_get_wtime();
    suma2=multiplica_matrices(M_A, M_B);  
    final=omp_get_wtime();  
	double tiempo_2_pro=final-empieza;
	
	
	omp_set_num_threads(4); 	
	printf("Dando valores a las matrices para 4 procesos  :\n\n");
   // inicializa_matrices(  M_A,   M_B);    	
	empieza=omp_get_wtime();
    suma4=multiplica_matrices(M_A, M_B);  
    final=omp_get_wtime();  
	double tiempo_4_pro=final-empieza;
	
	
   omp_set_num_threads(6); 	
	printf("Dando valores a las matrices para 6 procesos  :\n\n");
    inicializa_matrices(  M_A,   M_B);    	
	empieza=omp_get_wtime();
    suma6=multiplica_matrices(M_A, M_B);  
    final=omp_get_wtime();  
	double tiempo_6_pro=final-empieza;
	
	omp_set_num_threads(8); 	
	printf("Dando valores a las matrices para 8 procesos  :\n\n");
   // inicializa_matrices(  M_A,   M_B);    	
	empieza=omp_get_wtime();
    suma8=multiplica_matrices(M_A, M_B);  
    final=omp_get_wtime();  
	double tiempo_8_pro=final-empieza;
	
	
	//imprime_matrices( M_Res);
	printf("Tiempo sin planificacion  %fs resultado %fs:\n",tiempo_pro,suma1);
	printf("Tiempo planificacion 2 procesos %fs resultado %fs:\n",tiempo_2_pro,suma2);
	printf("Tiempo planificacion 4 procesos %fs resultado %fs:\n",tiempo_4_pro,suma4);
	printf("Tiempo planificacion 6 procesos %fs resultado %fs:\n",tiempo_6_pro,suma6);
	printf("Tiempo planificacion 8 procesos %fs resultado %fs:\n",tiempo_8_pro,suma8);
	
	
	//liberamos memoria
	free(M_A);
	free(M_B);

	
    return 0;
}

