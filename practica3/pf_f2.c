﻿/* CHM  29/11/2019 */
/* PRACTICA FINA   */
/* Practica Final*/
/* Codigo*/


#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define FIL  30248
#define COL  30248 /*tamaño maximo*/
#define DIMENSION 2096

int i,j,k;

void inicializa_matrices(float   * M_Res,float * M_A,float * M_B)
{
	
	 for(i=0;i<FIL;i++)
    {
        for(j=0;j<COL;j++)				
        {
			int pos=i;
			  M_A[pos]=0.0;
			  M_B[pos]=0.0;
			if (pos%2 == 0) 		  
			{		
               M_A[pos]=3.9f; // valores para A
		       M_B[pos]=1.4f; // valores para B
			}   
			else
			{	
			   M_A[pos]=3.5f; // valores para A
               M_B[pos]=1.2f; // valores para A		   
		    } 
		   
			M_Res[i]=0.0; 
        }
    }
	
}


void multiplica_matrices_secuencial(float   * M_Res,float * M_A,float * M_B)
{
   
   float electerm= 0.0;
   float dist= 0.0;
    float total_elec= 0.0;
  
   
   for(k=0;k<DIMENSION;k++)	   	
   {     
        for(i=0;i<FIL;i++)        
		   electerm= 0.0;
         for(j=0;j<COL;j++)
          {        	     
				 dist =1.5f;
				  electerm = (M_A[i]+M_B[j])/dist;				  				 
				  total_elec = total_elec +electerm;
		  } 
	M_Res[k]= total_elec;
		 total_elec= 0.0; 
		  
   }		  
	
}
void multiplica_matrices_primera(float   * M_Res,float * M_A,float * M_B)
{
	   	
  float electerm= 0.0;
   float dist= 0.0;
   float sum=0.0;
   float total_elec=0.0;
   float dato;
   
    #pragma omp parallel for private(i,k,j)  firstprivate(dato,electerm) shared(M_A,M_B) reduction(+:total_elec)
   for(k=0;k<DIMENSION;k++)	   	
   {     

    
        for(i=0;i<FIL;i++)        
		   electerm= 0.0;
	         dato= M_A[i] ;
			  dist =1.5f;
	   //  #pragma omp parallel for private(i) reduction(+:total_elec) firstprivate(dato,electerm)
         for(j=0;j<COL;j++)
          {        	     
				
				  electerm = (dato+M_B[j])/dist;				  				 
				  total_elec = total_elec +electerm;
		  } 
	M_Res[k]= total_elec;
		 total_elec= 0.0; 
		  
   }		  	
}


void multiplica_matrices_segunda(float   * M_Res,float * M_A,float * M_B)
{
	   	
  float electerm= 0.0;
   float dist= 0.0;
   float sum=0.0;
   float total_elec=0.0;
   
     #pragma omp parallel for private(dist,i,j) shared(M_A,M_B)
   for(k=0;k<DIMENSION;k++)	   	
   {     
        //#pragma omp parallel for private(dist,i,j) shared(M_A,M_B) reduction(+:total_elec) 
		for(i=0;i<FIL;i++)        		   	    
         for(j=0;j<COL;j++)
          {        	     
				 dist =1.5f;
				  total_elec += (M_A[i]+M_B[j])/dist;				  				 				 
		  } 
	M_Res[k]= total_elec;
		 total_elec= 0.0; 
		  
   }		  	
}

void multiplica_matrices_tercera(float   * M_Res,float * M_A,float * M_B)
{
	   	
  float electerm= 0.0;
   float dist= 0.0;
   float sum=0.0;
   float total_elec=0.0;
   
    #pragma omp parallel for  private(dist,i,j) shared(M_A,M_B) 
   for(k=0;k<DIMENSION;k++)	   	
   {     
       
		for(i=0;i<FIL;i++)        		   	    
         for(j=0;j<COL;j++)
          {        	     
				 dist =1.5f;
				  total_elec += (M_A[i]+M_B[j])/dist;				  				 				 
		  } 
	M_Res[k]= total_elec;
		 total_elec= 0.0; 
		  
   }		  	
}


void imprime_matrices(float   * M_Res)
{
    /* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n");
    for(i=0;i<FIL;i++)
    {
        for(j=0;j<COL;j++) {
			int pos=i*COL+j;
			 printf("%10f",M_Res[pos]);
		}
        printf("\n");
    }
}
void comprueba_matrices(float   * M_Res_sec,float   * M_Res_otra)
{
    int iguales=0;
	int iguales_otra=0;
	
	/* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n");
    for(i=0;i<FIL;i++)
          
			{
			int pos=i;
			
			 
			  if (M_Res_sec[pos] != M_Res_otra[pos])  
			  {
                  // printf("\n\nLa matriz resultado  y otra no seon iguales:\n\n");
				   iguales_otra+=1;

			  }
			}  
			
			if (iguales_otra == 0)
     		{
				printf("Solucion correcta\n");
				 printf("\n\nSecuencial %f  y Paralelo otra %f:",M_Res_sec[1516],M_Res_otra[1516]);
			}	
		
			else  {
				 printf("\nDiferencias %i  y Diferencias otra %i:",iguales,iguales_otra);
				 printf("\n\nSecuencial %f  y Paralelo otra %f:",M_Res_sec[0],M_Res_otra[0]);
			}
		
   
	
}	

int main(void)
{
	  float   * M_Res, * M_A, * M_B;
float   * M_Res_ocho;
float   * M_Res_otro;
float   * M_aux;
	
double final,empieza;

    printf("Programa Multiplica matrices :\n\n"); 
	printf("Reservando memoria :\n\n");
	
	   

    M_Res = (float *) malloc(FIL*sizeof(float));
	
	if (M_Res == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	   
	M_A = (float *) malloc(FIL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	M_B = (float *) malloc(FIL*sizeof(float));
	
	if (M_B	== NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	   
	M_Res_ocho= (float *) malloc(sizeof(float)*FIL);
    if (M_Res_ocho == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	M_Res_otro= (float *) malloc(sizeof(float)*FIL);
    if (M_Res_otro == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
   
  
	
	      
	omp_set_num_threads(1);		
	printf("Metodo multiplica reducion 1 :\n\n");
    inicializa_matrices(M_Res,M_A,M_B);      	    
	empieza=omp_get_wtime();		
    multiplica_matrices_secuencial(M_Res,  M_A,M_B); 
	final=omp_get_wtime();  
	double tiempo_pro=final-empieza;
	
	omp_set_num_threads(8);		
	printf("Metodo solucion primera :\n\n");
   // inicializa_matrices(M_Res_otro,M_A,M_B);      	    
	empieza=omp_get_wtime();		
    multiplica_matrices_primera(M_Res_otro,  M_A,M_B);  
	final=omp_get_wtime();  
	double tiempo_8_pro=final-empieza;
	
	comprueba_matrices(M_Res,M_Res_otro);      	    
    
/*    omp_set_num_threads(8);		
	printf("Metodo solucion segunda :\n\n");
    inicializa_matrices(M_Res_ocho,M_A,M_B);      	    
	empieza=omp_get_wtime();		
    multiplica_matrices_segunda(M_Res_ocho,  M_A,M_B);  
	final=omp_get_wtime();  
	double tiempo_6_pro=final-empieza; 	
	comprueba_matrices(M_Res,M_Res_ocho);    
	*/
	/*omp_set_num_threads(8);		
	printf("Metodo solucion tercera :\n\n");
    inicializa_matrices(M_Res_otro,M_A,M_B);      	    
	empieza=omp_get_wtime();		
    multiplica_matrices_segunda(M_Res_otro,  M_A,M_B);  
	final=omp_get_wtime();  
	double tiempo_3_pro=final-empieza; 	
	comprueba_matrices(M_Res,M_Res_otro);    */
	
	printf("\n Tiempo planificacion 1 procesos %fs resultado \n",tiempo_pro);
	printf("Tiempo primera solucion 8 procesos %fs resultado \n",tiempo_8_pro);
	//printf("Tiempo segunda solucion 8 procesos %fs resultado \n",tiempo_6_pro);/*
	//printf("Tiempo tercera solucion 8 procesos %fs resultado \n",tiempo_3_pro);*/
	
   
	
	//liberamos memoria*/
	free(M_A);
	free(M_B);
	free(M_Res);
	free(M_Res_ocho);
	free(M_Res_otro);
	
    return 0;
}

