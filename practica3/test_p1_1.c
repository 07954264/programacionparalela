/* CHM  30/10/2019 */
/* Practica 3   */
/* Ejercicio 1  */
/* Imprime el numero de proceso y numero total de procesos. */
#include <stdio.h>
#include <omp.h>
#define N 12345678

int i,procesos;
float x;
int main(void)
{
x=0.0;	  
printf("Programa Ejercicio 1 , practica 3"); 								
printf("Procesos:"); 								
scanf("%d", &procesos);
omp_set_num_threads(procesos);


#pragma omp paralle for private(i) schedule(guided) reduction(+:x)
 for (i=0;i<N;i++)
        {
	     
         x=x+i;  
    	// printf("Numero proceso %d de %d iteracion %d \n",omp_get_thread_num(),omp_get_num_threads(),i); 				}				
 
}	
    printf("\nValor de la operacion X %f\n",x); 
    return 0;
}

