/* CHM  20/10/2019 */
/* Practica 3*/
/* PARTE 2*/
/* Ejercicio 5*/
/* Multiplica matriz  por Vector */

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define FIL  40000
#define COL  40000


int i,j,pos;

void inicializa_matrices(float * M_A, float * M_B,float * M_Res)
{
	
	 for(i=0;i<COL;i++)
	 {	 		 
		 for(j=0;j<FIL;j++)
          {			
            M_A[i*COL+j]=2.0f; // valores para A
			M_B[j]=3.0f;
		    M_Res[j]=0;
		   }
	 }
	
}


void multiplica_matrices_vectores(float * M_A,float * M_B,float * M_Res)
{
	float a;
	float sum=0.0;
	a=M_A[0];
	#pragma omp parallel for  reduction(+:sum) private(j) ///? reduction probar	 
	  for(i=0;i<COL;i++)
	  {		 
		  for(j=0;j<FIL;j++)
           	      sum+= M_A[i*COL+j]*M_B[j];
		 M_Res[i]=sum;
           sum=0.0;		 
	  }	
        
	
}

void multiplica_matrices_secuencial(float * M_A,float * M_B,float * M_Res)
{
	float a;
	a=M_A[0];		
	  for(i=0;i<COL;i++)
	  {		 
		  for(j=0;j<FIL;j++)
           	      M_Res[i]+= M_A[i*COL+j]*M_B[j];
	  }	
        
	
}

void comprueba_matrices(float   * M_Res_sec,float   * M_Res_otra)
{
    int iguales=0;
	int iguales_otra=0;
	
	/* Mostramos el resultado */
    printf("La solucion es: ");
    for(i=0;i<FIL;i++)
	{ 
			
			int pos=i;						 
			  if (M_Res_sec[pos] != M_Res_otra[pos])  
			  {
                  // printf("\n\nLa matriz resultado  y otra no seon iguales:\n\n");
				   iguales_otra+=1;
			  }
    }			
			
			if (iguales_otra == 0)
     		{
				printf("Solucion correcta\n\n");
			}	
		
			else  {
				printf("****ERROR*****\n\n");
			}
			
		
   
	
}	

	

int main(void)
{
	  
								
float    * M_A, * M_B, * M_Res;
float    * M_Res_para;
double final,empieza;
float total;
float suma1,suma2,suma4,suma6,suma8;

    printf("Programa que multiplica vectores:\n\n"); 
	printf("Reservando memoria :\n\n");

	M_Res_para = (float *) malloc(COL*sizeof(float));
	
	if (M_Res_para == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	   
	M_A = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	M_B = (float *) malloc(COL*sizeof(float));
	
	if (M_B == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	M_Res= (float *) malloc(COL*sizeof(float));
	
	if (M_Res == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	 
   
	 printf("Producto escalar secuencial :\n\n");
	omp_set_num_threads(1); 	
	printf("Dando valores a las matrices para secuencial  :\n\n");
    inicializa_matrices(  M_A,   M_B,M_Res);    	
	empieza=omp_get_wtime();
    multiplica_matrices_secuencial(M_A,   M_B,M_Res);  
    final=omp_get_wtime();  
	double tiempo_pro=final-empieza;
	
	
	// printf("Producto escalar :\n\n");
	omp_set_num_threads(2); 	
	printf("\n\nDando valores a las matrices para 2 procesos  :\n");
    inicializa_matrices(  M_A,   M_B,M_Res_para);    	
	empieza=omp_get_wtime();
    multiplica_matrices_vectores(M_A,   M_B,M_Res_para);  
    final=omp_get_wtime();  
	double tiempo_2_pro=final-empieza;
	comprueba_matrices(M_Res,M_Res_para);    
	
    //printf("Producto escalar :\n\n");
	omp_set_num_threads(4); 	
	printf("Dando valores a las matrices para 4 procesos  :\n");
    inicializa_matrices(  M_A,   M_B,M_Res_para);    	
	empieza=omp_get_wtime();
    multiplica_matrices_vectores(M_A,   M_B,M_Res_para);  
    final=omp_get_wtime();  
	double tiempo_4_pro=final-empieza;
	comprueba_matrices(M_Res,M_Res_para);     
	
	// printf("Producto escalar :\n\n");
	omp_set_num_threads(6); 	
	printf("Dando valores a las matrices para 6 procesos  :\n");
    inicializa_matrices(  M_A,   M_B,M_Res_para);    	
	empieza=omp_get_wtime();
    multiplica_matrices_vectores(M_A,   M_B,M_Res_para);  
    final=omp_get_wtime();  
	double tiempo_6_pro=final-empieza;
	comprueba_matrices(M_Res,M_Res_para);     
	
	// printf("Producto escalar :\n\n");
	omp_set_num_threads(8); 	
	printf("Dando valores a las matrices para 8 procesos  :\n");
    inicializa_matrices(  M_A,   M_B,M_Res_para);    	
	empieza=omp_get_wtime();
    multiplica_matrices_vectores(M_A,   M_B,M_Res_para);  
    final=omp_get_wtime();  
	double tiempo_8_pro=final-empieza;
	
	comprueba_matrices(M_Res,M_Res_para);     
	
	//imprime_matrices( M_Res);
	printf("Tiempo planificacion 1 procesos %fs resultado \n",tiempo_pro);
	printf("Tiempo planificacion 2 procesos %fs resultado \n",tiempo_2_pro);
	printf("Tiempo planificacion 4 procesos %fs resultado \n",tiempo_4_pro);
	printf("Tiempo planificacion 6 procesos %fs resultado \n",tiempo_6_pro);
	printf("Tiempo planificacion 8 procesos %fs resultado \n",tiempo_8_pro);
	
	
	//liberamos memoria
	free(M_A);
	free(M_B);
	free(M_Res);
	free(M_Res_para);

	
    return 0;
}

