/* CHM  20/10/2019 */
/* Suma_vectores  */
/* Practica  3    */
/* Parte     1    */ 
/* Ejercicio 4    */
/* Saxpy */

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define FIL 1021312312


int i,j,pos;
int Alpha=2;

void inicializa_matrices(float * M_A, float * M_B)
{
	
	 for(i=0;i<FIL;i++)
    {
			pos=i; 
            M_A[i]=1.5f; // valores para A
			M_B[i]=3.0f; // valores para B
		   
        
    }
	
}

void saxpy(float * M_A,  float * M_B)
{
	 #pragma omp parallel for  private(i) shared(M_A,M_B) firstprivate(Alpha)
	 for(i=0;i<FIL;i++)
    {
        M_B[i]=M_A[i]*Alpha+M_B[i]; 
        
    }
	
}

void sin_saxpy(float * M_A,  float * M_B)
{
	
	 for(i=0;i<FIL;i++)
    {
        M_B[i]=M_A[i]*Alpha+M_B[i]; 
        
    }
	
}

void imprime_matrices(float   * M_Res)
{
    /* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(i=0;i<FIL;i++)
    {      
			 printf("%10f",M_Res[pos]);	 
    }
}
void comprueba_matrices(float   * M_Res_sec,float   * M_Res_otra)
{
    int iguales=0;
	int iguales_otra=0;
	
	/* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(i=0;i<FIL;i++)           
			{
			int pos=i;						 
			  if (M_Res_sec[pos] != M_Res_otra[pos])  
			  {
                  // printf("\n\nLa matriz resultado  y otra no seon iguales:\n\n");
				   iguales_otra+=1;
			  }
			}  
			
			if (iguales_otra == 0)
     		{
				printf("Solucion correcta\n");
				printf("\n\nSecuenciañ %f  y Paralelo otra %f:\n\n",M_Res_sec[0],M_Res_otra[0]);
			}	
		
			else  {
				 printf("***************ERRROR*************************\n");
				 printf("\n\nSecuenciañ %f  y Paralelo otra %f:\n\n",M_Res_sec[0],M_Res_otra[0]);
			}		   
}	

int main(void)
{
	  
								
float   * M_A, * M_B;
float   * M_Res_ocho;
float   * M_Res_otro;

    printf("Programa que suma vectores:\n\n"); 
	printf("Reservando memoria :\n\n");
	
	M_Res_ocho= (float *) malloc(FIL*sizeof(float));
	
	if (M_Res_ocho == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
  	
	   
	M_A = (float *) malloc(FIL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	M_B = (float *) malloc(FIL*sizeof(float));
	
	if (M_B == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	
	printf("Dando valores a las matrices :\n\n");
    inicializa_matrices(M_A,   M_B);      
    omp_set_num_threads(1); 
	double empieza=omp_get_wtime();
    sin_saxpy(M_A, M_B);  
    double final=omp_get_wtime();  
	double tiempo_2_pro=final-empieza;
	
    omp_set_num_threads(8); 
	printf("Dando valores a las matrices para 8 procesos :\n\n");
    inicializa_matrices( M_A,   M_Res_ocho);    	
	
	empieza=omp_get_wtime();
    saxpy(  M_A, M_Res_ocho);  
	
    final=omp_get_wtime();  	
	double tiempo_8_pro=final-empieza;
	
	comprueba_matrices(M_Res_ocho,M_B); 
	
	

	//imprime_matrices( M_Res);
	//printf("Tiempo sin paralelizar %fs :\n",tiempo_sin_pro);
	printf("Tiempo  procesos %fs :\n",tiempo_2_pro);
	printf("Tiempo 8 procesos %fs :\n",tiempo_8_pro);
	
	//liberamos memoria
	free(M_A);
	free(M_B);
	
	
    return 0;
}

