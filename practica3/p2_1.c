/* CHM  07/11/2019 */
/* Practica 3 */
/* Ejercicio 1 */
/*PARTE 2  Ejercicio 1 */
#include <stdio.h>
#include <omp.h>
#define N  100000

int i,x,procesos;

 void tarea1()
 {
	int i;
    double x ,resultado,sum,pi,step;
	 
    step=1.0/N;

	 for(i=0;i<N;i++)
       {
			x= (i-0.5)*step;
			sum= sum+ 4.0/(1.0+x*x);
       }			
	    pi=step*sum;
  }	 
 
 void tarea2()
 {
	int i;
    double x ,resultado,sum,pi,step;
	 
    step=1.0/N;

	 for(i=0;i<N;i++)
       {
			x= (i-0.5)*step;
			sum= sum+ 4.0/(1.0+x*x);
       }			
	    pi=step*sum;
  }	 
 
 
int main(void)
{
x=0;	  
double pi;
double step,sum;
double empieza,final,tiempo_2_pro;

double empieza_sc1,final_sc1,tiempo_2_pro_sc1;
double empieza_sc2,final_sc2,tiempo_2_pro_sc2;

printf("Programa Ejercicio 1 PARTE 2 \n"); 								
printf("Procesos:"); 								
scanf("%d", &procesos);

omp_set_num_threads(procesos);

    empieza=omp_get_wtime();
	printf("Ejecutando tarea 1\n");
	tarea1();
	printf("Ejecutando tarea 2\n");
	tarea2();
	final=omp_get_wtime();  
	tiempo_2_pro=final-empieza;						
    printf(" Timepo sin paralelizar:%fs \n",tiempo_2_pro);


#pragma omp parallel
{
   empieza_sc1=omp_get_wtime();
   #pragma omp sections
   {{
	printf("Ejecutando tarea 1\n");
	tarea1();
	final_sc1=omp_get_wtime();  
	tiempo_2_pro_sc1=final_sc1-empieza_sc1;	
    
   
   empieza_sc2=omp_get_wtime();
    }
   #pragma omp section
   {
	printf("Ejecutando tarea 2\n");
	tarea1();
	final_sc2=omp_get_wtime();  
	tiempo_2_pro_sc2=final_sc2-empieza_sc2;	
   }
 }	
}
	
    printf(" Tiempo con secciones :%fs \n",tiempo_2_pro_sc1+tiempo_2_pro_sc2);



    return 0;
}

