/* CHM  07/11/2019 */
/* Practica 3 */
/* Ejercicio 6  */
#include <stdio.h>
#include <omp.h>
#define N  100000

int i,x,procesos;

int main(void)
{
x=0;	  
double pi;
double step,sum;

printf("Programa Ejercicio 6 , practica 3"); 								
printf("Procesos:"); 								
scanf("%d", &procesos);
step=1.0/N;
omp_set_num_threads(procesos);

#pragma omp parallel
{

#pragma omp master
  printf("Numero proceso master %d de %d    \n", omp_get_thread_num(),omp_get_num_threads()); 
 
 #pragma omp barrier
  
  printf("Numero proceso hijo %i de %i  \n",omp_get_thread_num(),omp_get_num_threads()); 


}	
    return 0;
}

