/* CHM  03/11/2019 */
/* reduce_Vectores  */
/* practica 3*/
/* Ejercicio 5*/
/*Reduce Vector*/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define num_steps 100000


int i,j,pos;

double step;
	

int main(void)
{
 

int i;
double x ,pi,sum;
int procesos;
sum=0.0; 

	step=1.0/num_steps;
	double empieza=omp_get_wtime();
	#pragma omp parallel for  shared(step) reduction(+:sum) 
	{
	 for(i=0;i<num_steps;i++)
       {
			x= (i-0.5)*step;
			sum= sum+ 4.0/(1.0+x*x);
       }			
	  procesos=omp_get_num_threads();
	}  
		double final=omp_get_wtime();  
	   double tiempo_2_pro=final-empieza;						
       pi=step*sum;
	   
	printf("Procesos %d:\n",procesos);   
	printf("Resultado para pi procesos %f:\n",pi);
	printf("Tiempo  %fs :\n",tiempo_2_pro);
	
	
	
    return 0;
}

