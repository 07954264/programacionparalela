/* CHM  03/11/2019 */
/* reduce_Vectores  */
/* practica 3*/
/* Ejercicio 5*/
/*Reduce Vector*/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

#define num_steps 10000000

int i,j,pos;

double step;

float  calcula_pi()
{
	int i;
double x ,resultado,sum,pi;
sum=0.0; 

step=1.0/num_steps;
#pragma omp parallel for  private(x) firstprivate(step) reduction(+:sum) 
	 for(i=0;i<num_steps;i++)
       {
			x= (i-0.5)*step;
			sum= sum+ 4.0/(1.0+x*x);
       }			
	    pi=step*sum;
	   return pi;
	
}

	

int main(void)
{
 
float resultado;
	double empieza,final,tiempo_2_pro;

    omp_set_num_threads(1); 
	empieza=omp_get_wtime();
    resultado=calcula_pi();
	final=omp_get_wtime();  
	tiempo_2_pro=final-empieza;						
    printf("Resultado para 1 proceso %f: timepo:%fs \n",resultado,tiempo_2_pro);
	
	 omp_set_num_threads(2); 
	empieza=omp_get_wtime();
    resultado=calcula_pi();
	final=omp_get_wtime();  
	tiempo_2_pro=final-empieza;						
    printf("Resultado para 2 procesos %f: timepo:%fs \n",resultado,tiempo_2_pro);
	
	 omp_set_num_threads(4); 
	empieza=omp_get_wtime();
    resultado=calcula_pi();
	final=omp_get_wtime();  
	tiempo_2_pro=final-empieza;						
    printf("Resultado para 4 process %f: timepo:%fs \n",resultado,tiempo_2_pro);
	
	omp_set_num_threads(6); 
	empieza=omp_get_wtime();
    resultado=calcula_pi();
	final=omp_get_wtime();  
	tiempo_2_pro=final-empieza;						
    printf("Resultado para 6 procesos %f: timepo:%fs \n",resultado,tiempo_2_pro);
	
	omp_set_num_threads(8); 
	empieza=omp_get_wtime();
    resultado=calcula_pi();
	final=omp_get_wtime();  
	tiempo_2_pro=final-empieza;						
    printf("Resultado para 8 proceso %f: timepo:%fs \n",resultado,tiempo_2_pro);
	
	
    return 0;
}

