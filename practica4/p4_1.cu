﻿/* CHM  08/12/2019 */
/* METODO STENCIL  */
/* Practica 4*/
/* Ejercicio 1*/
/* Paralelizar Stencil 1d*/
/* Enunciado:
	Ejercicio 1.- Los patrones “Stencil” están muy presentes en algoritmos computacionales para el
	cálculo de ecuaciones diferenciales. Un patrón “Stencil” es una disposición geométrica de un grupo
	que se relaciona con un punto de interés. Esta geometría puede ser de n dimensiones.
	Escribe un kernel CUDA usando la técnica de tiling para un patrón Stencil 1-D donde el cálculo del
	punto de interés se realice mediante la siguiente fórmula:
	
				𝑦[i] = (𝑦[i − 1] + 𝑦[i] + y[i + 1])/3 
	
	Escriba también la llamada al kernel, indicando el número de hilos y de threads que ejecutarán el
	mismo. 
	
	Preguntas
	¿Crees qué es interesante aplicar tiling en este código? ¿Por qué?

*/	




#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"


#define FIL 1024

#define DIMENSION 2048
#define ANCHOBLOQUE 256


__global__ void stencil1d ( float *M_Res_d, float *M_A_d)
{

	int indice=blockIdx.x * blockDim.x + threadIdx.x;
	    
	   if  ( (indice > 0) && (indice < (FIL-1)) )       
	     {        
			 M_Res_d[indice]=0.3f*(M_A_d[indice-1]+M_A_d[indice] +M_A_d[indice+1]); 
		
         }

}

void imprime_matrices(float   * M_Res)
{
    /* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(int i=0;i<FIL;i++)
    {      
		 printf("%10f",M_Res[i]);
	 }
        printf("\n");
    
}

void comprueba_matrices(float   * M_Res_otra,float   * M_Res_sec)
{
    
	int iguales_otra=0;
	
	/* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(int i=1;i<FIL-1;i++)
	 {
	
			  if (M_Res_sec[i] != M_Res_otra[i])  
			  {
                  // printf("\n\nLa matriz resultado  y otra no seon iguales:\n\n");
				   iguales_otra+=1;
			  }
	 }  
			
			if (iguales_otra == 0)
     		{
				printf("Solucion correcta\n");
				printf("\nSecuencial %f  y Paralelo otra %f:\n",M_Res_sec[5],M_Res_otra[5]);
				
			}	
		
			else  {
				printf("*****ERROR*********\n");
				printf("\nSecuencial %f  y Paralelo otra %f:\n",M_Res_sec[5],M_Res_otra[5]);
			}		   
}		





void inicializa_matrices(float   * M_Res,float * M_A)
{
	int pos;
	 for(int i=0;i<FIL;i++)
    {
        pos=i;
			if (pos%2 == 0) 
               M_A[i]=111111.0f; // valores para A
			else
			   M_A[i]=223333.0f; // valores para A	
		
        
    }
	
}

void metodo_stencil_secuencial(float   * M_Res,float * M_A)
{

       for(int i=1;i<FIL-1;i++)        
       
          {        					
		    M_Res[i]=0.3*(M_A[i-1]+M_A[i] +M_A[i+1]); 
         }
		 			
}

int main(void)
{
	  
								
float   * M_Res, * M_A,* M_Res_secuencial;
float   * M_Res_d, * M_A_d;

float   * M_aux =NULL;
	


int tipo_device=0;   
int total_hilos=0;
int hilos_bloque;
float mils=0;
float mils_pa=0;

cudaEvent_t start,stop;
cudaEvent_t start_pa,stop_pa;
cudaError_t cudaStatus;

cudaEventCreate(&start);
cudaEventCreate(&stop);
cudaEventCreate(&start_pa);
cudaEventCreate(&stop_pa);

printf("Programa Stencil :\n\n"); 
printf("Reservando memoria :\n\n");
	
M_Res = (float *) malloc(FIL*sizeof(float));
	
if (M_Res == NULL) 
    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
    }
	
M_Res_secuencial = (float *) malloc(FIL*sizeof(float));
	
if (M_Res_secuencial == NULL) 
    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
    }	

M_A = (float *) malloc(FIL*sizeof(float));
	
if (M_A == NULL) 
    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
     }
		   


/////////////////////////////////////////////////////////////////////////////GPU///////////////////////////////////////////////////////////////
//seleccionamos device
tipo_device=0;   
cudaSetDevice(tipo_device); 
	//0 - Tesla K40 
	//1 - Tesla K20 
	
//reservamos memoria GPU
cudaMalloc(   &M_Res_d, FIL * sizeof(float));
cudaMalloc(   &M_A_d, FIL * sizeof(float));

//pasamos datos de host to device
cudaMemcpy(M_Res_d,M_Res,FIL * sizeof(float), cudaMemcpyHostToDevice);
cudaMemcpy(M_A_d,M_A,FIL * sizeof(float), cudaMemcpyHostToDevice);
	
total_hilos=FIL;
hilos_bloque=ANCHOBLOQUE;

//dimgrid (x)
dim3 dimGrid(ceil((total_hilos+hilos_bloque-1)/hilos_bloque));
dim3 dimBlock(hilos_bloque); // hilos por bloque
	
//Definir numero de hilos y bloques
printf("bloques: %d\n", (int)ceil(total_hilos/hilos_bloque)+1);
printf("hilos por bloque: %d\n", hilos_bloque);	

printf("Dando valores a las matrices:\n\n");
inicializa_matrices(M_Res,M_A);      	

cudaEventRecord(start_pa,0);


for(int k=0;k<DIMENSION ;k++)
    {
		
    //llamamos a kernel
    stencil1d <<< dimGrid ,dimBlock>>> (M_Res_d,M_A_d);
	 
	    M_aux=M_A_d;
		M_A_d=M_Res_d;
		M_Res_d=M_aux;	    
    }
cudaEventRecord(stop_pa,0);

cudaEventElapsedTime(&mils_pa,start_pa,stop_pa);

//control de errores kernel
cudaDeviceSynchronize();
cudaStatus = cudaGetLastError();
if(cudaStatus != cudaSuccess) fprintf(stderr, "Error en el kernel %d\n", cudaStatus); 

//Traemos info al host
cudaMemcpy(M_Res,M_Res_d,FIL * sizeof(float), cudaMemcpyDeviceToHost);
cudaMemcpy(M_A,M_A_d,    FIL * sizeof(float), cudaMemcpyDeviceToHost);

//Liberamos memoria reservada para GPU
cudaFree(M_Res_d);
cudaFree(M_A_d);
///////////////////////////////////////////////////////////////////////FIN GPU///////////////////////////////////////////////////////////////    
	


//empezamos secuencial
printf("Dando valores a las matrices secuencial:\n\n");
inicializa_matrices(M_Res_secuencial,M_A);
cudaEventRecord(start,0);
	float   * M_aux2 =NULL;
for(int k=0;k<DIMENSION;k++)
 {
   metodo_stencil_secuencial(M_Res_secuencial,  M_A);  
	 
	    M_aux2=M_A;
		M_A=M_Res_secuencial;
		M_Res_secuencial=M_aux2;
		
 }

cudaEventRecord(stop,0);
cudaEventElapsedTime(&mils,start,stop);    
//comprueba_matrices( M_Res);  
 
	
printf("Tiempo secuencial %fs resultado \n",mils);
printf("Tiempo GPU %fs resultado \n",mils_pa);
	
//liberamos memoria
free(M_A);
free(M_Res);
free(M_Res_secuencial);
	
return 0;
}

