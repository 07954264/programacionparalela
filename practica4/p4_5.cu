﻿/* CHM  13/12/2019 */
/* reduce Vectores  */
/* Practica 4*/
/* Ejercicio 4   */
/* Suma matrices */
/* 
		Dado el siguiente esqueleto de código CUDA, escriba un kernel utilizando la técnica
		de tiling “sumaMatrices”, que calcule la suma de dos matrices A y B y las almacene en una matriz
		C. Utilice para ello la siguiente plantilla de código:
*/
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "cuda_runtime.h"


#define FIL  2048
#define COL  2048 /*tamaño maximo*/
#define DIMENSION 2048
#define ANCHOBLOQUE 16





void inicializa_matrices(float   * M_Res,float * M_A,float * M_B)
{
	
	 for(int i=0;i<FIL;i++)
    {
        for(int j=0;j<COL;j++)				
        {
			int pos=i*COL+j;
			
			if (pos%2 == 0) 		  
			{		
               M_A[pos]=1; // valores para A
		       M_B[pos]=1; // valores para B
			}   
			else
			{	
			   M_A[pos]=1.0f; // valores para A
               M_B[pos]=1.0f; // valores para A		   
		    } 
		   
			M_Res[pos]=0.0; 
        }
    }
	
}

void suma_matrices_secuencial(float   * M_Res,float * M_A,float * M_B)
{
	   	
	    
       for(int i=0;i<DIMENSION;i++)        
         for(int j=0;j<DIMENSION;j++)
          {        
	        int pos=j*FIL+i;
				  M_Res[pos]=M_A[pos]+M_B[pos];
		        
		           
							
         }
	
}


__global__  void sum_con_tiling (float * M_Res_d,float * M_A_d,float * M_B_d)
{	
	__shared__  float a_s[ANCHOBLOQUE][ANCHOBLOQUE];
	__shared__  float b_s[ANCHOBLOQUE][ANCHOBLOQUE];
		
	int tx= threadIdx.x;
	int ty= threadIdx.y;	
	int bx= blockIdx.x;
	int by= blockIdx.y;
	
	float suma=0.0;
		
	int i =   blockIdx.x * blockDim.x + threadIdx.x;
	int j =   blockIdx.y * blockDim.y + threadIdx.y;
	
	 int indice=i*FIL+j;
		
	if (tx < ANCHOBLOQUE && ty<ANCHOBLOQUE && i < DIMENSION && j <DIMENSION)
	 {
	
            a_s[ty][tx] =M_A_d[indice];
	        b_s[ty][tx] =M_B_d[indice];
	       __syncthreads();
  	
	       M_Res_d[indice]=a_s[ty][tx]+b_s[ty][tx];		        	    		
		   __syncthreads();
	   
	 } 
   
	 
    
	 
}


__global__  void sum_sin_tiling (float * M_Res_d,float * M_A_d,float * M_B_d)
{	
	int tx= threadIdx.x;
	int ty= threadIdx.y;	
	int bx= blockIdx.x;
	int by= blockIdx.y;
	
	
	int i =   blockIdx.x * blockDim.x + threadIdx.x;
	int j =   blockIdx.y * blockDim.y + threadIdx.y;
	
	//int fila =   by*FIL*ANCHOBLOQUE +ty*FIL;
    //int columna =bx*FIL*ANCHOBLOQUE +tx;
	
	 int indice=i*FIL+j;
	
	if (indice < FIL*FIL)
	{	        
          M_Res_d[indice]=M_A_d[indice] + M_B_d[indice];
	}
 				
	   
}


void comprueba_matrices(float   * M_Res_sec,float   * M_Res_otra)
{
    int iguales=0;
	int iguales_otra=0;
	int pos;
	/* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(int i=0;i<FIL;i++)
           for(int j=0;j<COL;j++)
			{
	           pos=i*FIL+j;						 
			  if (M_Res_sec[pos] != M_Res_otra[pos])  
			  {
                  // printf("\n\nLa matriz resultado  y otra no seon iguales:\n\n");
				   iguales_otra=pos;
			  }
			}  
			
			if (iguales_otra == 0)
     		{
				printf("Solucion correcta\n");
				printf("\n\nSecuencial %f  y Paralelo otra %f:\n\n",M_Res_sec[0],M_Res_otra[0]);
			}	
		
			else  {
				 printf("***************ERRROR*************************\n");
				 printf("\n\nSecuencial %f  y Paralelo otra %f: posicion:%d  \n",M_Res_sec[pos],M_Res_otra[pos],pos);
			}		   
}		
	

int main(void)
{
	  
								
float   * M_Res, * M_A, * M_B;
float   * M_Res_d, * M_A_d, * M_B_d;
float   * M_Res_secuencial;
float   * M_aux;
	
int tipo_device=0;   
int hilos_bloque=0; 
int total_hilos=0;

float mils=0;
float mils_pa=0;
float mils_pa_tiling=0;
int grid_n;
	
cudaEvent_t start,stop;
cudaEvent_t start_pa,stop_pa;
cudaEvent_t start_pa_tiling,stop_pa_tiling;
cudaError_t cudaStatus;

cudaEventCreate(&start);
cudaEventCreate(&stop);
cudaEventCreate(&start_pa);
cudaEventCreate(&stop_pa);
cudaEventCreate(&start_pa_tiling);
cudaEventCreate(&stop_pa_tiling);	

    printf("Programa Multiplica matrices :\n\n"); 
	printf("Reservando memoria secuencial:\n\n");
	
	
	M_Res_secuencial= (float *) malloc(sizeof(float)*FIL*COL);
    if (M_Res_secuencial == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }

    M_Res = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_Res == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	   
	M_A = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	M_B = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_B	== NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
  
	
	//secuencial      
	printf("Metodo multiplica secuencial :\n\n");
    inicializa_matrices(M_Res_secuencial,M_A,M_B);      	    
	cudaEventRecord(start,0);		
    suma_matrices_secuencial(M_Res_secuencial,  M_A,M_B);  
	cudaEventRecord(stop,0);	  
	cudaEventElapsedTime(&mils,start,stop);

	///////////////////////////////////////////////////////////////CUDA GPU///////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////SIN tiling////////////////////////////////////////////////////////////////
    //seleccionamos device
	tipo_device=0;   
	cudaSetDevice(tipo_device); 
		//0 - Tesla K40 
		//1 - Tesla K20 
	
	inicializa_matrices(M_Res,M_A,M_B);
	
	//reservamos memoria GPU
	cudaMalloc( (void **)  &M_B_d,FIL * COL * sizeof(float));
	cudaMalloc( (void **)  &M_A_d,FIL * COL * sizeof(float));
    cudaMalloc( (void **)  &M_Res_d,FIL * COL *sizeof(float));
	
	//pasamos datos de host to device
	cudaMemcpy(M_Res_d,M_Res,FIL * COL * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(M_B_d,M_B,FIL * COL * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(M_A_d,M_A,FIL * COL * sizeof(float), cudaMemcpyHostToDevice);
	
	total_hilos=FIL;
	hilos_bloque=ANCHOBLOQUE;

	//dimgrid (x,y)
	dim3 dimGrid(ceil((total_hilos+hilos_bloque-1)/hilos_bloque),ceil((total_hilos+hilos_bloque-1)/hilos_bloque));
	dim3 dimBlock(hilos_bloque,hilos_bloque); // hilos por bloque 32 *32 =1024
	
	//Definir numero de hilos y bloques
	printf("bloques: %d\n", (int)ceil(total_hilos/hilos_bloque)+1);
	printf("hilos por bloque: %d\n", hilos_bloque);	

	printf("Dando valores a las matrices:\n\n");
	inicializa_matrices(M_Res,M_A,M_B);      	

	cudaEventRecord(start_pa,0);	
    //llamamos a kernel
    sum_sin_tiling <<< dimGrid ,dimBlock>>> (M_Res_d,M_A_d,M_B_d);	 
	cudaEventRecord(stop_pa,0);
	 cudaEventSynchronize(stop_pa);
	mils_pa =0.0f;

	cudaEventElapsedTime(&mils_pa,start_pa,stop_pa);

	//control de errores kernel
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if(cudaStatus != cudaSuccess) fprintf(stderr, "Error en el kernel %d\n", cudaStatus); 

	//Traemos info al host
	cudaMemcpy(M_Res,M_Res_d,FIL * COL * sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(M_B,M_B_d,    FIL * COL * sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(M_A,M_A_d,    FIL * COL * sizeof(float), cudaMemcpyDeviceToHost);

	//Liberamos memoria reservada para GPU
	cudaFree(M_Res_d);
	cudaFree(M_A_d);
	cudaFree(M_B_d);
   	
    ///////////////////////////////////////////////////////////// FIN SIN tiling/////////////////////////////////////////////////////////////
    //compruebo resultados
	comprueba_matrices(M_Res_secuencial,M_Res);

   
	///////////////////////////////////////////////////////////////CUDA GPU///////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////CON tiling////////////////////////////////////////////////////////////////
    
    //seleccionamos device
	tipo_device=0;   
	cudaSetDevice(tipo_device); 
		//0 - Tesla K40 
		//1 - Tesla K20 
	
	inicializa_matrices(M_Res,M_A,M_B);
	
	//reservamos memoria GPU
	cudaMalloc( (void **)  &M_B_d,FIL * COL * sizeof(float));
	cudaMalloc( (void **)  &M_A_d,FIL * COL * sizeof(float));
    cudaMalloc( (void **)  &M_Res_d,FIL * COL *sizeof(float));
	
	//pasamos datos de host to device
	cudaMemcpy(M_Res_d,M_Res,FIL * COL * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(M_B_d,M_B,FIL * COL * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(M_A_d,M_A,FIL * COL * sizeof(float), cudaMemcpyHostToDevice);
	
	total_hilos=FIL;
	hilos_bloque=ANCHOBLOQUE;

	//dimgrid (x,y)
	// dimGrid(ceil((total_hilos+hilos_bloque-1)/hilos_bloque),ceil((total_hilos+hilos_bloque-1)/hilos_bloque));
	// dimBlock(hilos_bloque,hilos_bloque); // hilos por bloque 32 *32 =1024
	
	//Definir numero de hilos y bloques
	printf("bloques: %d\n", (int)ceil(total_hilos/hilos_bloque)+1);
	printf("hilos por bloque: %d\n", hilos_bloque);	

	printf("Dando valores a las matrices:\n\n");
	inicializa_matrices(M_Res,M_A,M_B);      	

	cudaEventRecord(start_pa_tiling,0);	
    //llamamos a kernel
    sum_con_tiling <<< dimGrid ,dimBlock>>> (M_Res_d,M_A_d,M_B_d);	 
	cudaEventRecord(stop_pa_tiling,0);
	 cudaEventSynchronize(stop_pa_tiling);
	mils_pa_tiling =0.0f;

	cudaEventElapsedTime(&mils_pa_tiling,start_pa_tiling,stop_pa_tiling);

	//control de errores kernel
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if(cudaStatus != cudaSuccess) fprintf(stderr, "Error en el kernel %d\n", cudaStatus); 

	//Traemos info al host
	cudaMemcpy(M_Res,M_Res_d,FIL * COL * sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(M_B,M_B_d,    FIL * COL * sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(M_A,M_A_d,    FIL * COL * sizeof(float), cudaMemcpyDeviceToHost);

	//Liberamos memoria reservada para GPU
	cudaFree(M_Res_d);
	cudaFree(M_A_d);
	cudaFree(M_B_d);
   	
	
	
    ///////////////////////////////////////////////////////////// FIN CON tiling/////////////////////////////////////////////////////////////
	comprueba_matrices(M_Res_secuencial,M_Res);
	
	//tiempos
	printf("\nTiempo planificacion secuencial: %fs  \n",mils);
	printf("Tiempo planificacion        cuda:  %fs  \n",mils_pa);	
    printf("Tiempo planificacion cuda   tiling:  %fs  \n",mils_pa_tiling);
	
	
	//liberamos memoria*/
	free(M_A);
	free(M_B);
	free(M_Res);
	free(M_Res_secuencial);
	
	
    return 0;
}