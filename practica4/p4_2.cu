﻿/* CHM  09/12/2019 */
/* Practica  4   */
/* Ejercicio     2    */ 
/* Saxpy */
/*	El código SAXPY (Single-Precision real Alpha X Plus Y) es una operación a nivel de
	vector incluida en la famosa librería Basic Linear Algebra Subprograms (BLAS). SAXPY es una
	combinación de multiplicación escalar y suma de vectores, definida por la fórmula:
   
      𝑦 	𝑦  y = &x𝛼𝑥 + y𝑦
	Escriba un kernel CUDA que resuelva SAXPY, incluyendo cabecera del kernel y argumentos del
	mismo. Escriba también la llamada al kernel, indicando el número de hilos y de threads que
	ejecutarán el mismo.
	
	NOTA: Suponga que el tamaño de x e y es N. ¿Crees qué es interesante aplicar tiling en este
	código? ¿Por qué?*/
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include "cuda_runtime.h"

#define FIL  2048

#define DIMENSION 2048
#define ANCHOBLOQUE 32
#define ALPHA 2


__global__ void saxpy_cuda(float * M_A_d,  float * M_B_d)
{	 
	int i=blockIdx.x * blockDim.x + threadIdx.x;
	 
	 if  ( i < FIL)      
	  {  
        M_B_d[i]=M_A_d[i]*ALPHA+M_B_d[i];         
      }
	
}

void sin_saxpy(float * M_A,  float * M_B)
{
	
	 for(int i=0;i<FIL;i++)
    {
        M_B[i]=M_A[i]*ALPHA+M_B[i]; 
        
    }
	
}

void imprime_matrices(float   * M_Res)
{
    /* Mostramos el resultado */
int pos;
    printf("\n\nLa matriz resultado es:\n\n");
    for(int i=0;i<FIL;i++)
    {      
			 printf("%10f",M_Res[pos]);	 
    }
}
void comprueba_matrices(float   * M_Res_sec,float   * M_Res_otra)
{
    int iguales=0;
	int iguales_otra=0;
	
	/* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(int i=0;i<FIL;i++)           
			{
			int pos=i;						 
			  if (M_Res_sec[pos] != M_Res_otra[pos])  
			  {
                  // printf("\n\nLa matriz resultado  y otra no seon iguales:\n\n");
				   iguales_otra+=1;
			  }
			}  
			
			if (iguales_otra == 0)
     		{
				printf("Solucion correcta\n");
				printf("\n\nSecuencial %f  y Paralelo otra %f:\n\n",M_Res_sec[0],M_Res_otra[0]);
			}	
		
			else  {
				printf("***************ERRROR*************************\n");
				printf("\n\nSecuencial %f  y Paralelo otra %f:\n\n",M_Res_sec[0],M_Res_otra[0]);
			}		   
}	



void inicializa_matrices(float * M_A, float * M_B)
{
	int pos;
	 for(int i=0;i<FIL;i++)
    {
			pos=i; 
            M_A[i]=1.5f; // valores para A
			M_B[i]=3.0f; // valores para B		          
    }
	
}
int main(void)
{

	float   * M_A, * M_B;
	float   * M_Res_otro;
	float   * M_A_d, * M_B_d;

	

int tipo_device;
int total_hilos;
int hilos_bloque;
int anchobloque;

float mils=0;
float mils_pa=0;

cudaEvent_t start,stop;
cudaEvent_t start_pa,stop_pa;
cudaError_t cudaStatus;

cudaEventCreate(&start);
cudaEventCreate(&stop);
cudaEventCreate(&start_pa);
cudaEventCreate(&stop_pa);

    printf("Programa que saxpy:\n"); 
	printf("Reservando memoria :\n");
	
	M_Res_otro= (float *) malloc(FIL*sizeof(float));
	
	if (M_Res_otro == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
  	
	   
	M_A = (float *) malloc(FIL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	M_B = (float *) malloc(FIL*sizeof(float));
	
	if (M_B == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
	
	printf("Dando valores a las matrices secuencial:\n");
    inicializa_matrices(M_A,   M_B);      
    cudaEventRecord(start,0);
    sin_saxpy(M_A, M_B);  
    cudaEventRecord(stop,0);
	cudaEventElapsedTime(&mils,start,stop);
	
	M_Res_otro=M_B;
	
	printf("Dando valores a las matrices cuda:\n");
	inicializa_matrices(M_A,   M_B);    
	
	//////////////////////////////////////////////////////////////GPU/////////////////////////////////////////////////////
	//seleccionamos device
	tipo_device=0;   
	cudaSetDevice(tipo_device); 
	//0 - Tesla K40 
	//1 - Tesla K20 
	
	//reservamos memoria GPU
	cudaMalloc( (void **)  &M_B_d,FIL * sizeof(float));
	cudaMalloc( (void **)  &M_A_d,FIL * sizeof(float));

	//pasamos datos de host to device
	cudaMemcpy(M_B_d,M_B,FIL * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(M_A_d,M_A,FIL * sizeof(float), cudaMemcpyHostToDevice);
	
	total_hilos=FIL;
	hilos_bloque=ANCHOBLOQUE;

	//dimgrid (x)
	dim3 dimGrid(ceil((total_hilos+hilos_bloque-1)/hilos_bloque));
	dim3 dimBlock(hilos_bloque); // hilos por bloque
	
	//Definir numero de hilos y bloques
	printf("bloques: %d\n", (int)ceil(total_hilos/hilos_bloque)+1);
	printf("hilos por bloque: %d\n", hilos_bloque);	
      	
	cudaEventRecord(start_pa,0);
		
    //llamamos a kernel
    saxpy_cuda <<< dimGrid ,dimBlock>>> (M_A_d,M_B_d);
	 	
	cudaEventRecord(stop_pa,0);

	cudaEventElapsedTime(&mils_pa,start_pa,stop_pa);

	//control de errores kernel
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if(cudaStatus != cudaSuccess) fprintf(stderr, "Error en el kernel %d\n", cudaStatus); 

	//Traemos info al host
	cudaMemcpy(M_B,M_B_d,FIL * sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(M_A,M_A_d,    FIL * sizeof(float), cudaMemcpyDeviceToHost);

	//Liberamos memoria reservada para GPU
	cudaFree(M_B_d);
	cudaFree(M_A_d);
	////////////////////////////////////////////////////////////FIN GPU//////////////////////////////////////////////////////////////////
    
	comprueba_matrices(M_Res_otro, M_B);
    
	//imprime_matrices( M_Res);
	//printf("Tiempo sin paralelizar %fs :\n",tiempo_sin_pro);
	printf("Tiempo secuencial %fs :\n",mils);
	printf("Tiempo GPU %fs :\n",mils_pa);
	
	//liberamos memoria
	free(M_A);
	free(M_B);
	
	
    return 0;
}

