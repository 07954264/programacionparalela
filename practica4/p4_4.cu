﻿/* CHM  13/12/2019 */
/* reduce Vectores  */
/* Practica 4*/
/* Ejercicio 4   */
/* multiplicar matrices */
/* 
		Dado el siguiente esqueleto de código CUDA, escriba un kernel utilizando la técnica
		de tiling “matrixMult”, que calcule la multiplicación de dos matrices A y B y almacene 
		el resultado en una matriz C.
*/
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#define DIMENSION  1024
#define ANCHOBLOQUE 16

__global__  void mul_con_tail (float * M_Res_d,float * M_A_d,float * M_B_d)
{	
	__shared__  float a_s[ANCHOBLOQUE][ANCHOBLOQUE];
	__shared__  float b_s[ANCHOBLOQUE][ANCHOBLOQUE];
		
	int tx= threadIdx.x;
	int ty= threadIdx.y;	
	int bx= blockIdx.x;
	int by= blockIdx.y;
	
	float suma =0.0f;
	
	
  
	int fila =   by*ANCHOBLOQUE*DIMENSION +    ty*DIMENSION+tx;
    int columna =bx*ANCHOBLOQUE     +ty*DIMENSION                       +tx;
	
 //if (fila+columna < DIMENSION*DIMENSION )
 //{ 
	int fila_aux=fila;
	int col_aux =columna;
	 
	 for (int i=0;i<(DIMENSION/ANCHOBLOQUE);i++) 
     {
        a_s[ty][tx] =M_A_d[fila];
	    b_s[ty][tx] =M_B_d[columna];
	    __syncthreads();
		
  	
		for(int k=0;k<ANCHOBLOQUE;k++){
				 suma+=a_s[ty][k]*b_s[k][tx];
				 
		   }     	  
		if ( i <  (DIMENSION/ANCHOBLOQUE) -1 )
		 {
		   fila+=ANCHOBLOQUE;
		   columna+=ANCHOBLOQUE*DIMENSION;	
		  } 
		  
		__syncthreads();
		
		// M_Res_d[fila+columna] = suma;	 
	  }
	if  (fila_aux + col_aux < DIMENSION*DIMENSION)
	 M_Res_d[fila_aux+col_aux] = suma;	
   // if  (tx == 0 && ty==0 && bx==0 && by == 0)
	 //  printf("dato %d,%d, %d,%d, %d,%d,%d,%d, --\n",tx,bx,ty,by,fila,columna,fila_aux,col_aux);
//}	 
}



void inicializa_matrices(float   * M_Res,float * M_A,float * M_B)
{
	int pos;
	 for(int i=0;i<DIMENSION;i++)
    {
        for(int j=0;j<DIMENSION;j++)				
        {
			 pos=i*DIMENSION+j;
			
			if (pos%2 == 0) 		  
			{		
                        M_A[pos]=1.0f; // valores para A
		       M_B[pos]=1.f; // valores para B
			}   
			else
			{	
			   M_A[pos]=1.0f; // valores para A
                         M_B[pos]=1.0f; // valores para A		   
		        } 
		   
			M_Res[pos]=0.0f;   
      }	
}
}


void multiplica_matrices_secuencial(float   * M_Res,float * M_A,float * M_B)
{
	   	
	    
       for(int i=0;i<DIMENSION;i++)        
         for(int j=0;j<DIMENSION;j++)
          {        
	        float sum=0.0;			
			  for(int k=0;k<DIMENSION;k++)
				  sum+=M_A[i*DIMENSION+k]*M_B[k*DIMENSION+j];
		        
		    M_Res[i*DIMENSION+j]=sum;
							
         }
	
}



__global__  void mul_sin_tail (float * M_Res_d,float * M_A_d,float * M_B_d)
{	
	
     
	
    int i =   blockIdx.x * blockDim.x + threadIdx.x;
	int j =   blockIdx.y * blockDim.y + threadIdx.y;
 
	float suma=0.0f;

  	if  (j < DIMENSION &&  i <DIMENSION )
	 {
	 for(int k=0;k<DIMENSION;k++)
				  suma+=M_A_d[i*DIMENSION+k]*M_B_d[k*DIMENSION+j];
		        
		   M_Res_d[i*DIMENSION+j]=suma;
	 }
}



void imprime_matrices(float   * M_Res)
{
    /* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(int i=0;i<DIMENSION;i++)
    {
        for(int j=0;j<DIMENSION;j++) {
			int pos=i*DIMENSION+j;
			 printf("%10f",M_Res[pos]);
		}
        printf("\n");
    }
}
void comprueba_matrices(float   * M_Res_sec,float   * M_Res_otra)
{
  
	int iguales_otra=0;
	
	/* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(int i=0;i<DIMENSION;i++)
           for(int j=0;j<DIMENSION;j++)
			{
			int pos=i*DIMENSION+j;						 
			  if (M_Res_sec[pos] != M_Res_otra[pos])  
			  {
                  // printf("\n\nLa matriz resultado  y otra no seon iguales:\n\n");
				   iguales_otra+=1;
			  }
			}  
			
			if (iguales_otra == 0)
     		{
				printf("Solucion correcta\n");
				printf("\nSecuencial %f  y Paralelo otra %f:\n",M_Res_sec[0],M_Res_otra[0]);
			}	
		
			else  {
				// printf("");
				 printf("\nSecuencial %f  y Paralelo otra %f:\n",M_Res_sec[0],M_Res_otra[0]);
			}		   
}		

int main(void)
{
	  
								
float   * M_Res, * M_A, * M_B;
float   * M_Res_d, * M_A_d, * M_B_d;
float   * M_Res_secuencial;

int tipo_device=0;   
int hilos_bloque=0; 
int total_hilos=0;

float mils=0;
float mils_pa=0;
float mils_pa_tiling=0;

	
cudaEvent_t start,stop;
cudaEvent_t start_pa,stop_pa;
cudaEvent_t start_pa_tiling,stop_pa_tiling;
cudaError_t cudaStatus;

cudaEventCreate(&start);
cudaEventCreate(&stop);
cudaEventCreate(&start_pa);
cudaEventCreate(&stop_pa);
cudaEventCreate(&start_pa_tiling);
cudaEventCreate(&stop_pa_tiling);


    printf("Programa Multiplica matrices :\n"); 
	printf("Reservando memoria secuencial:\n");
	
	
	M_Res_secuencial= (float *) malloc(sizeof(float)*DIMENSION*DIMENSION);
    if (M_Res_secuencial == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }

    M_Res = (float *) malloc(DIMENSION*DIMENSION*sizeof(float));
	
	if (M_Res == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	   
	M_A = (float *) malloc(DIMENSION*DIMENSION*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	M_B = (float *) malloc(DIMENSION*DIMENSION*sizeof(float));
	
	if (M_B	== NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
  
	
	//secuencial      
	printf("Metodo multiplica secuencial :\n");
    inicializa_matrices(M_Res_secuencial,M_A,M_B);      	    
	cudaEventRecord(start,0);		
    multiplica_matrices_secuencial(M_Res_secuencial,  M_A,M_B);  
	cudaEventRecord(stop,0);	  
	cudaEventSynchronize(stop);
	mils =0.0f;

	cudaEventElapsedTime(&mils,start,stop);

	///////////////////////////////////////////////////////////////CUDA GPU///////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////SIN TILING////////////////////////////////////////////////////////////////
    //seleccionamos device
	tipo_device=0;   
	cudaSetDevice(tipo_device); 
		//0 - Tesla K40 
		//1 - Tesla K20 
	
	printf("Metodo multiplica sin tiling :\n");
	inicializa_matrices(M_Res,M_A,M_B);
	
	//reservamos memoria GPU
	cudaMalloc( (void **)  &M_B_d,DIMENSION * DIMENSION * sizeof(float));
	cudaMalloc( (void **)  &M_A_d,DIMENSION * DIMENSION * sizeof(float));
    cudaMalloc( (void **)  &M_Res_d,DIMENSION * DIMENSION *sizeof(float));
	
	//pasamos datos de host to device
	cudaMemcpy(M_Res_d,M_Res,DIMENSION * DIMENSION * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(M_B_d,M_B,DIMENSION * DIMENSION * sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(M_A_d,M_A,DIMENSION * DIMENSION * sizeof(float), cudaMemcpyHostToDevice);
	
	total_hilos=DIMENSION;
	hilos_bloque=ANCHOBLOQUE;

	//dimgrid (x,y)
	dim3 dimGrid(ceil(total_hilos/hilos_bloque),ceil(total_hilos/hilos_bloque));
	dim3 dimBlock(hilos_bloque,hilos_bloque); // hilos por bloque 32 *32 =1024
	
	//Definir numero de hilos y bloques
	printf("bloques: %d\n", (int)ceil(total_hilos/hilos_bloque)+1);
	printf("hilos por bloque: %d\n", hilos_bloque);	

	printf("Dando valores a las matrices:\n\n");
	inicializa_matrices(M_Res,M_A,M_B);      	

	cudaEventRecord(start_pa,0);	
    //llamamos a kernel
    mul_con_tail <<< dimGrid ,dimBlock>>> (M_Res_d,M_A_d,M_B_d);	 
	cudaEventRecord(stop_pa,0);
	cudaEventSynchronize(stop_pa);
	mils_pa =0.0f;


	cudaEventElapsedTime(&mils_pa,start_pa,stop_pa);

	//control de errores kernel
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if(cudaStatus != cudaSuccess) fprintf(stderr, "Error en el kernel %d\n", cudaStatus); 

	//Traemos info al host
	cudaMemcpy(M_Res,M_Res_d,DIMENSION * DIMENSION * sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(M_B,M_B_d,    DIMENSION * DIMENSION * sizeof(float), cudaMemcpyDeviceToHost);
	cudaMemcpy(M_A,M_A_d,    DIMENSION * DIMENSION * sizeof(float), cudaMemcpyDeviceToHost);

	//Liberamos memoria reservada para GPU
	cudaFree(M_Res_d);
	cudaFree(M_A_d);
	cudaFree(M_B_d);
   	
    ///////////////////////////////////////////////////////////// FIN /////////////////////////////////////////////////////////////
	
    ////////////////compruebo resultados
	printf("Resultado multiplica con tiling :\n");
	comprueba_matrices(M_Res_secuencial,M_Res);
	//imprime_matrices(M_Res);


	
	//tiempos
	printf("\nTiempo planificacion secuencial: %fs  \n",mils);
    printf("Tiempo planificacion cuda   tiling:  %fs  \n",mils_pa);
	
	
	//liberamos memoria*/
	free(M_A);
	free(M_B);
	free(M_Res);
	free(M_Res_secuencial);
	
	
    return 0;
}

