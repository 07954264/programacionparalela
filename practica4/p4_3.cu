﻿/* CHM  11/12/2019 */
/* reduce Vectores  */
/* Practica 4*/
/* Ejercicio 3   */
/*Reduce Vector*/

/* 
  Realizar un código CUDA que realice la reducción de un vector. La reducción de un
  vector consiste en obtener un elemento significativo de todos los elementos del vector como indica
  la figura 1 (máximo, mínimo o suma). 
  Inicialice los valores del array de enteros y obtenga la suma	de todos los elementos
*/	

#include <stdio.h>
#include <omp.h>
#include <math.h>
#include "cuda_runtime.h"


#define FIL 102411
#define ANCHOBLOQUE 1024


__global__  void rvec (float * M_A_d)
{	
	__shared__ float idata[1024];
    __shared__ float suma;
	
    float tmp; 
    int col =   blockIdx.x * blockDim.x + threadIdx.x;
 
	int tidx = threadIdx.x;

  	idata[tidx]=0;
	
	
	  if  (col < FIL )
		{
       
         idata[tidx]=M_A_d[col];
     		  __syncthreads();
       
	     //LO REDUCIMOS	
	    //reducion en bloques de 2
		for (int s = blockDim.x / 2; s > 0; s >>= 1) 
		{
			if (tidx < s) 
			{
                idata[tidx] += idata[tidx + s];
			}
			__syncthreads();
		}
  
		// write result for this block to global mem
		//OBTENEMOS EL VALOR EN resultado , todos los bloques.
		if (tidx == 0) 
		   {
		    
			
			tmp=idata[0];
			
			atomicAdd(&(M_A_d[0]),tmp);   
			//printf("bloque %d: suma %f:\n" ,blockIdx.x, tmp);
			
			 }
			 
			 
		
       }//fin IF
	 
	
	
}

void inicializa_Vectores(float * M_A)
{
	
	 for(int i=0;i<FIL;i++)
    {
			 
            M_A[i]=1; // valores para A

    }
	
	//menos el primero que dejo el resultado para 10 FIL sera 9 resultado
	M_A[0]=0; // valores para A
}



float sin_reduce_Vectores(float * M_A)
{
	float suma=0;
	 for(long i=0;i<FIL;i++)
    {
        suma +=M_A[i];
        
    }
	return suma;
}


int main(void)
{
float resultado_suma;	  
								
float    * M_A;
float    * M_A_d;


int tipo_device=0;   
int hilos_bloque=0; 
int total_hilos=0;

float mils=0;
float mils_pa=0;
int grid_n;


cudaEvent_t start,stop;
cudaEvent_t start_pa,stop_pa;
cudaError_t cudaStatus;

cudaEventCreate(&start);
cudaEventCreate(&stop);
cudaEventCreate(&start_pa);
cudaEventCreate(&stop_pa);

//seleccionamos device
tipo_device=0;   
cudaSetDevice(tipo_device); 
	//0 - Tesla K40 
	//1 - Tesla K20 
	

    printf("Programa que reduce vectores:\n\n"); 
	printf("Reservando memoria :\n\n");

  	   
	M_A = (float *) malloc(FIL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	
			
    inicializa_Vectores( M_A);    	
	cudaEventRecord(start,0);
    resultado_suma=sin_reduce_Vectores(  M_A);  
    cudaEventRecord(stop,0);	
	cudaEventElapsedTime(&mils,start,stop);
	
		
	//reservamos memoria GPU	
	cudaMalloc( (void **)  &M_A_d,FIL * sizeof(float));

	//pasamos datos de host to device	
	cudaMemcpy(M_A_d,M_A,FIL * sizeof(float), cudaMemcpyHostToDevice);
	
	total_hilos=FIL;//FIL;
	hilos_bloque=ANCHOBLOQUE;//TAMBLOCK;
     grid_n=(int) ceil((total_hilos+hilos_bloque-1)/hilos_bloque);
	 
	//dimgrid (x)
	dim3 dimGrid(grid_n);
	dim3 dimBlock(hilos_bloque); // hilos por bloque
	
	//Definir numero de hilos y bloques
	printf("bloques: %d\n", (int)ceil((total_hilos+hilos_bloque-1)/hilos_bloque));
	printf("hilos por bloque: %d\n", hilos_bloque);	
    
    cudaEventRecord(start_pa,0);
    //llamamos a kernel
    rvec <<< dimGrid ,dimBlock >>> (M_A_d);     
	
    cudaEventRecord(stop_pa,0);	
	cudaEventElapsedTime(&mils_pa,start_pa,stop_pa);   	
	
	 
	//control de errores kernel
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if(cudaStatus != cudaSuccess) fprintf(stderr, "Error en el kernel %d\n", cudaStatus); 

	cudaMemcpy(M_A,M_A_d,FIL * sizeof(float), cudaMemcpyDeviceToHost);
	
	//Liberamos memoria reservada para GPU
	cudaFree(M_A_d);
	////////////////////////////////////////////////////////////FIN GPU//////////////////////////////////////////////////////////////////
        
	//imprime_matrices( M_Res);	
	printf("Resultado secuencial %f:\n",resultado_suma);
	printf("Tiempo secuencial %fs :\n",mils);
	printf("Resultado GPU %f:\n",M_A[0]);
	printf("Tiempo GPU %fs :\n",mils_pa);
	
	
	//liberamos memoria
	free(M_A);
	
	
	
    return 0;
}

