﻿/* CHM  14/12/2019 */
/* reduce Vectores  */
/* Practica 4*/
/* Ejercicio 6   */
/* Paralelizar JAcobi*/
/* 
		El método de Jacobi es un algoritmo popular para la solución de la ecuación de
		Laplace en un dominio diferencial cuadrado regularmente discretizado. El núcleo se basa en la
		siguiente idea: Consideremos un cuerpo representado por una matriz 2D de partículas, cada una
		con un valor inicial de temperatura. Este cuerpo está en contacto con un valor fijo de temperatura
		en los cuatro límites, y la ecuación de Laplace se resuelve para todos los puntos internos a
		determinar su temperatura como la media de las cuatro partículas vecinas. Toma esta tarea
		computacional como el núcleo, un número de iteraciones se realizan sobre los datos para volver a
		calcular las temperaturas medias repetidamente, y los valores convergen gradualmente a una
		solución más fina hasta la precisión deseada
		
		for (k=0; k<4096;k++) {
               for (i=1; i<(N-1);i++) {
                     for (j=1; j<(N-1);j++) {
                           A[i][j] = 0.2f*(B[i][j]+)B[i-1][j]+ B[i][j-1]+ B[i+1][j]+ B[i][j+1]);

*/
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "cuda_runtime.h"

#define FIL  256
#define COL  256 /*tamaño maximo*/
#define DIMENSION 256
#define ANCHOBLOQUE 32



void inicializa_matrices(float   * M_Res,float * M_A)
{
	
	 for(int i=0;i<FIL;i++)
    {
        for(int j=0;j<COL;j++)				
        {
			int pos=i*COL+j; 
			if (pos%2 == 0) 
               M_A[pos]=111175.0f; // valores para A
			else
			   M_A[pos]=11150.0f; // valores para A	
			//M_Res[pos]=12.0; 
        }
    }
	
}

void metodo_jacobi_secuencial(float * M_Res,float * M_A)
{
	

      
       for(int i=1;i<FIL-1;i++)        
         for(int j=1;j<COL-1;j++)
          {        
			int pos=i*COL+j;
			
			int posI_ant=(i-1)*COL+j;
			int posI_pos=(i+1)*COL+j;
			int posJ_ant=i*COL+(j+1);
			int posJ_pos=i*COL+(j-1);
			
		    M_Res[pos]=0.2 *(M_A[pos]+M_A[posI_ant]
			                        +M_A[posI_pos]
									+M_A[posJ_ant]
                                    +M_A[posJ_pos]
							); 
         }
		 
		 			
}

__global__  void sin_jacobi_secuencial (float * M_Res_d,float * M_A_d,float * M_B_d)
{	
	
	
    int i =   blockIdx.x * blockDim.x + threadIdx.x;
	int j =   blockIdx.y * blockDim.y + threadIdx.y;
 
    
	    int pos=i*COL+j;
			
			int posI_ant=(i-1)*COL+j;
			int posI_pos=(i+1)*COL+j;
			int posJ_ant=i*COL+(j+1);
			int posJ_pos=i*COL+(j-1);
			
		if  (i < DIMENSION-1 &&  i> 0 && j < DIMENSION-1 &&  j> 0 )		
		    M_Res_d[pos]=0.2 *(M_A_d[pos]+M_A_d[posI_ant]
			                        +M_A_d[posI_pos]
									+M_A_d[posJ_ant]
                                    +M_A_d[posJ_pos]);
			
	
	
}



void comprueba_matrices(float   * M_Res_sec,float   * M_Res_otra)
{
    int iguales=0;
	int iguales_otra=0;
	
	/* Mostramos el resultado */
    printf("\n\nLa matriz resultado es:\n\n");
    for(int i=0;i<FIL;i++)
           for(int j=0;j<COL;j++)
			{
			int pos=i*FIL+j;						 
			  if (M_Res_sec[pos] != M_Res_otra[pos])  
			  {
                  // printf("\n\nLa matriz resultado  y otra no seon iguales:\n\n");
				   iguales_otra+=1;
			  }
			}  
			
			if (iguales_otra == 0)
     		{
				printf("Solucion correcta\n");
				printf("\n\nSecuencial %f  y Paralelo otra %f:\n\n",M_Res_sec[5],M_Res_otra[5]);
				
			}	
		
			else  {
			
				 printf("\n\nSecuencial %f  y Paralelo otra %f:\n\n",M_Res_sec[2],M_Res_otra[2]);
			}		   
}		

int main(void)
{
	  
								
float   * M_Res, * M_A,* M_B;
float   * M_Res_d, * M_A_d,* M_B_d;
float   * M_Res_secuencial;
float   * M_aux;

int k;
	
int tipo_device=0;   
int hilos_bloque=0; 
int total_hilos=0;

float mils=0;
float mils_pa=0;
float mils_pa_tail=0;
int grid_n;
	
cudaEvent_t start,stop;
cudaEvent_t start_pa,stop_pa;
cudaEvent_t start_pa_tail,stop_pa_tail;
cudaError_t cudaStatus;

cudaEventCreate(&start);
cudaEventCreate(&stop);
cudaEventCreate(&start_pa);
cudaEventCreate(&stop_pa);
cudaEventCreate(&start_pa_tail);
cudaEventCreate(&stop_pa_tail);	

    printf("Programa Multiplica matrices :\n\n"); 
	printf("Reservando memoria secuencial:\n\n");
	
	
	M_Res_secuencial= (float *) malloc(sizeof(float)*FIL*COL);
    if (M_Res_secuencial == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }

    M_Res = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_Res == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	   
	M_A = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_A == NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
	M_B = (float *) malloc(FIL*COL*sizeof(float));
	
	if (M_B	== NULL) 
	    {
		fprintf(stderr,"Error al reservar memoria \n");
		return -1;		
	   }
  
	
	//secuencial      
	printf("Metodo multiplica secuencial :\n\n");
    inicializa_matrices(M_Res_secuencial,M_A);      	    
	cudaEventRecord(start,0);		
    
     for(k=0;k<DIMENSION ;k++)
      {
       //llamamos a kernel
       metodo_jacobi_secuencial (M_Res,M_A);
	   M_aux=M_A;
	   M_A=M_Res;
	   M_Res=M_aux;	    
      }
	
	cudaEventRecord(stop,0);	  
	cudaEventElapsedTime(&mils,start,stop);

	///////////////////////////////////////////////////////////////CUDA GPU///////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////SIN TAIL////////////////////////////////////////////////////////////////
    //seleccionamos device
	tipo_device=0;   
	cudaSetDevice(tipo_device); 
		//0 - Tesla K40 
		//1 - Tesla K20 
	
	inicializa_matrices(M_Res,M_A);
	
	//reservamos memoria GPU	
	cudaMalloc( (void **)  &M_A_d,	FIL * COL * sizeof(float));
    cudaMalloc( (void **)  &M_Res_d,FIL * COL *	sizeof(float));
	
	//pasamos datos de host to device
	cudaMemcpy(M_Res_d,M_Res,FIL * COL * sizeof(float), cudaMemcpyHostToDevice);	
	cudaMemcpy(M_A_d,M_A,	 FIL * COL * sizeof(float), cudaMemcpyHostToDevice);
	
	total_hilos=FIL;
	hilos_bloque=ANCHOBLOQUE;

	//dimgrid (x,y)
	dim3 dimGrid(ceil((total_hilos+hilos_bloque-1)/hilos_bloque),ceil((total_hilos+hilos_bloque-1)/hilos_bloque));
	dim3 dimBlock(hilos_bloque,hilos_bloque); // hilos por bloque 32 *32 =1024
	
	//Definir numero de hilos y bloques
	printf("bloques: %d\n", (int)ceil(total_hilos/hilos_bloque)+1);
	printf("hilos por bloque: %d\n", hilos_bloque);	

	printf("Dando valores a las matrices:\n\n");
	inicializa_matrices(M_Res,M_A);      	

	cudaEventRecord(start_pa,0);	
    //llamamos a kernel
    
       for(k=0;k<DIMENSION ;k++)
      {
       //llamamos a kernel
         sin_jacobi_secuencial <<< dimGrid ,dimBlock>>> (M_Res_d,M_A_d,M_B_d);	
		 M_aux=M_A_d;
	   M_A_d=M_Res_d;
	   M_Res_d=M_aux;	     
      }	
	cudaEventRecord(stop_pa,0);

	cudaEventElapsedTime(&mils_pa,start_pa,stop_pa);

	//control de errores kernel
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if(cudaStatus != cudaSuccess) fprintf(stderr, "Error en el kernel %d\n", cudaStatus); 

	//Traemos info al host
	cudaMemcpy(M_Res,M_Res_d,FIL * COL * sizeof(float), cudaMemcpyDeviceToDevice);	
	cudaMemcpy(M_A,M_A_d,    FIL * COL * sizeof(float), cudaMemcpyDeviceToDevice);

	//Liberamos memoria reservada para GPU
	cudaFree(M_Res_d);
	cudaFree(M_B_d);
	cudaFree(M_A_d);
	
   	
    ///////////////////////////////////////////////////////////// FIN /////////////////////////////////////////////////////////////
    //compruebo resultados
	comprueba_matrices(M_Res_secuencial,M_Res);


	
	//tiempos
	printf("\nTiempo planificacion secuencial: %fs  \n",mils);
	printf("Tiempo planificacion        cuda:  %fs  \n",mils_pa);	
    
	
	
	//liberamos memoria*/
	free(M_A);
	free(M_Res);
	free(M_Res_secuencial);
	
	
    return 0;
}

